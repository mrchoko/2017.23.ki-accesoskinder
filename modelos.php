<?php
require_once "config/errores.php";
require_once "config/conexion.php";
class Modelos extends Errores{

	public function filtra_campos_base($valor, $tabla){

        $conexion = new Conexion();
        $conexion->selecciona_base_datos();
        $link = $conexion->link;

        $status = "";
        if($valor == 'activo'){
        	$status = $status." OR a.status = 1";
        }
        elseif($valor == 'inacitivo'){
        	$status = $status." OR a.status = 0";
        }

        $consulta = "
        	SELECT a.id,a.descripcion, a.status 
        	FROM $tabla as a 
        	WHERE a.descripcion LIKE '%$valor%' OR a.id = '$valor' $status";
        $result = $link->query($consulta);

        while( $row = mysqli_fetch_assoc( $result)){
		    $new_array[] = $row; // Inside while loop
		}
        if (!empty($new_array)) {
			return $new_array;
		}
		else{
			return array();	
		}

	}

	public function activa_db($id, $tabla){
		
		$estructura = $this->genera_estructura();
	    if(!array_key_exists($tabla, $estructura)){ 
	    	$this->error(32,__LINE__,__FILE__);
	    	return false;
		}
		$conexion = new Conexion();
		$conexion->selecciona_base_datos();
		$link = $conexion->link;
		$link->query("UPDATE $tabla SET status = '1' WHERE id = $id");
		if($link->error){
			$this->error(41,__LINE__,__FILE__);
			return false;
		}
		else{

			return True;
		}
	}

	public function alta_db($registro=False, $tabla=False, $nombre_base_datos=False){ //Teminado
		$this->error(-1,__LINE__,__FILE__);
		
		if(is_array($registro)){
			$conexion = new Conexion();
			$conexion->selecciona_base_datos($nombre_base_datos);
			$link = $conexion->link;
		    $estructura = $this->genera_estructura();
		    
		    if(!array_key_exists($tabla, $estructura)){ //errro validado
		    	$this->error(32,__LINE__,__FILE__);
		    }
		    else{
		    	$tabla_base = $estructura[$tabla]['campos'];
		    	if(!$this->valida_campos_nulos($registro, $tabla_base)){
		    		$this->error(37,__LINE__,__FILE__);
		    	}
		    	if(!$this->valida_campos_obligatorios($tabla_base, $registro)){
		    		$this->error(38,__LINE__,__FILE__);
		    	}
		    }
		}
		else{ // error validado
			$this->error(24,__LINE__,__FILE__);
		}
		if($this->numero_error){
			return false;
		}
		else{
			$valores = $this->genera_valores_insercion($tabla_base,$registro);
			$campos = $this->genera_campos_insercion($tabla_base);
			$consulta_insercion = "INSERT INTO ". $tabla." (".$campos.") VALUES (".$valores.")";
			$link->query($consulta_insercion);
			if($link->error){
				$this->error(36,__LINE__,__FILE__);
				return false;
			}
			else{
				$this->error(-1,__LINE__,__FILE__);
				$registro_id = $link->insert_id;
				return $registro_id;
			}
		}
	}

	public function desactiva_db($id, $tabla){
		
		$estructura = $this->genera_estructura();
	    if(!array_key_exists($tabla, $estructura)){ 
	    	$this->error(32,__LINE__,__FILE__);
	    	return false;
		}
		$conexion = new Conexion();
		$conexion->selecciona_base_datos();
		$link = $conexion->link;
		$link->query("UPDATE $tabla SET status = '0' WHERE id = $id");
		if($link->error){
			$this->error(41,__LINE__,__FILE__);
			return false;
		}
		else{

			return True;
		}
	}

	public function elimina_db($tabla=False, $id=False, $nombre_base_datos=False){ 
		
		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);
		$link = $conexion->link;
		$modelo = new Modelos();

		$database = $modelo->genera_estructura();
		if(!array_key_exists($tabla, $database)){
			$this->error(32,__LINE__,__FILE__);
			return false;
		}
		
		$tabla_base = $database[$tabla]['campos'];
		$consulta = "DELETE FROM ".$tabla. " WHERE id = ".$id;

		$result = $link->query($consulta);

		if($link->error){
			$this->error(40,__LINE__,__FILE__);
			return false;
		}
		return $id;

	}

	public function genera_campo_insercion($campos, $campo, $i, $n_campos){
		$campos = $campos." ".$campo;
		if($i<$n_campos){
			$campos = $campos.",";
		}
		return $campos;
	}

	public function genera_campos_insercion($tabla_base){
		$campos = "";
		$n_campos = count($tabla_base);
		$i = 1;
		foreach ($tabla_base as $campo => $atributos) {
			$campos = $this->genera_campo_insercion($campos, $campo, $i, $n_campos);
			$i++;
		}
		return $campos;		
	}

	public function genera_estructura(){
		$id = array(
			'nombre_campo'=>'id', 'tipo_dato' => 'int(11)', 'nulo' => 'NOT NULL', 'primary' => 'PRIMARY KEY',
			'autoincrement' => 'AUTO_INCREMENT');
		$descripcion = array(
			'nombre_campo'=>'descripcion', 'tipo_dato' => 'varchar(500)', 'nulo' => 'NOT NULL', 
			'primary' => false, 'autoincrement' => false);
		$observaciones = array(
			'nombre_campo'=>'observaciones', 'tipo_dato' => 'text', 'nulo' => false, 'primary' => false, 
			'autoincrement' => false);
		$status = array(
			'nombre_campo'=>'status', 'tipo_dato' => 'tinyint(1)', 'nulo' => false, 'primary' => false, 
			'autoincrement' => false);

		$grupo['nombre_tabla'] = 'grupo';
		$grupo['campos'] = array(
			'id'=>$id, 'descripcion'=>$descripcion, 'observaciones'=>$observaciones, 'status'=>$status);

		$seccion['nombre_tabla'] = 'seccion';
		$seccion['campos'] = array(
			'id'=>$id, 'descripcion'=>$descripcion, 'observaciones'=>$observaciones, 'status'=>$status);

	    $user = array(
	    	'nombre_campo' => 'user','tipo_dato' => 'varchar(500)','nulo' => 'NOT NULL','primary' => false,'autoincrement' => false);
	    $password = array(
	    	'nombre_campo' => 'password','tipo_dato' => 'varchar(50)','nulo' => 'NOT NULL','primary' => false,'autoincrement' => false);
	    $email = array(
	    	'nombre_campo' => 'email','tipo_dato' => 'varchar(500)','nulo' => 'NOT NULL','primary' => false,'autoincrement' => false);
	    $grupo_id = array(
			'nombre_campo'=>'grupo_id', 'tipo_dato' => 'int(11)', 'nulo' => 'NOT NULL', 'primary' => false,
			'autoincrement' => false);

	    $usuario['nombre_tabla'] = 'usuario';
		$usuario['campos'] = array(
			'id'=>$id, 'user'=>$user, 'password'=>$password, 'email'=>$email,'grupo_id'=>$grupo_id);

    	$seccion_id = array(
			'nombre_campo'=>'seccion_id', 'tipo_dato' => 'int(11)', 'nulo' => 'NOT NULL', 'primary' => false,
			'autoincrement' => false);

    	$accion['nombre_tabla'] = 'accion';
		$accion['campos'] = array(
			'id'=>$id, 'descripcion'=>$descripcion, 'seccion_id'=>$seccion_id);

    	$accion_id = array(
			'nombre_campo'=>'accion_id', 'tipo_dato' => 'int(11)', 'nulo' => 'NOT NULL', 'primary' => false,
			'autoincrement' => false);

    	$accion_grupo['nombre_tabla'] = 'accion_grupo';
		$accion_grupo['campos'] = array(
			'id'=>$id, 'accion_id'=>$accion_id, 'grupo_id'=>$grupo_id);

		$madre_padre_tutor['nombre_tabla'] = 'madre_padre_tutor';
		
		$persona_autorizada['nombre_tabla'] = 'persona_autorizada';

		$alumno['nombre_tabla'] = 'alumno';

		$nombre = array(
			'nombre_campo'=>'nombre', 'tipo_dato' => 'varchar(500)', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$apellido_paterno = array(
			'nombre_campo'=>'apellido_paterno', 'tipo_dato' => 'varchar(500)', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$apellido_materno = array(
			'nombre_campo'=>'apellido_materno', 'tipo_dato' => 'varchar(500)', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$fecha_nacimiento = array(
			'nombre_campo'=>'fecha_nacimiento', 'tipo_dato' => 'date', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$foto = array(
			'nombre_campo'=>'foto', 'tipo_dato' => 'longblob', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$telefono = array(
			'nombre_campo'=>'telefono', 'tipo_dato' => 'varchar(15)', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$codigo_barras = array(
			'nombre_campo'=>'codigo_barras', 'tipo_dato' => 'varchar(15)', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$tipo_sangre = array(
			'nombre_campo'=>'tipo_sangre', 'tipo_dato' => 'varchar(15)', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$genero = array(
			'nombre_campo'=>'genero', 'tipo_dato' => 'varchar(20)', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$lugar_nacimiento = array(
			'nombre_campo'=>'lugar_nacimiento', 'tipo_dato' => 'text', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$fecha_ingreso = array(
			'nombre_campo'=>'fecha_ingreso', 'tipo_dato' => 'date', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$domicilio = array(
			'nombre_campo'=>'domicilio', 'tipo_dato' => 'text', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);


		$madre_padre_tutor['campos'] = array(
			'id' => $id, 'nombre' => $nombre, 'apellido_paterno' => $apellido_paterno, 'apellido_materno' => $apellido_materno, 'fecha_nacimiento' => $fecha_nacimiento, 'foto' => $foto, 'observaciones' => $observaciones, 'status' => $status, 'telefono' => $telefono, 'codigo_barras' => $codigo_barras, 'domicilio' => $domicilio );

		$persona_autorizada['campos'] = array(
			'id' => $id, 'nombre' => $nombre, 'apellido_paterno' => $apellido_paterno, 'apellido_materno' => $apellido_materno, 'fecha_nacimiento' => $fecha_nacimiento, 'foto' => $foto, 'observaciones' => $observaciones, 'status' => $status, 'telefono' => $telefono, 'codigo_barras' => $codigo_barras, 'domicilio' => $domicilio);

		$alumno['campos'] = array(
			'id' => $id, 'nombre' => $nombre, 'apellido_paterno' => $apellido_paterno, 'apellido_materno' => $apellido_materno, 'fecha_nacimiento' => $fecha_nacimiento, 'foto' => $foto, 'observaciones' => $observaciones, 'status' => $status, 'codigo_barras' => $codigo_barras, 'tipo_sangre' => $tipo_sangre, 'genero' => $genero, 'lugar_nacimiento' => $lugar_nacimiento, 'fecha_ingreso' => $fecha_ingreso);

		$token['nombre_tabla'] = 'token';

		$usuario_id = array(
			'nombre_campo'=>'usuario_id', 'tipo_dato' => 'int(11)', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$nombre_usuario = array(
			'nombre_campo'=>'nombre_usuario', 'tipo_dato' => 'varchar(500)', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$token_campo = array(
			'nombre_campo'=>'token', 'tipo_dato' => 'text', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);

		$token['campos'] = array(
			'id' => $id, 'usuario_id' => $usuario_id, 'grupo_id' => $grupo_id, 'nombre_usuario' => $nombre_usuario, 'token' => $token_campo);

		$puesto = array(
			'nombre_campo'=>'puesto', 'tipo_dato' => 'varchar(50)', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$hora_entrada = array(
			'nombre_campo'=>'hora_entrada', 'tipo_dato' => 'text', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$hora_salida = array(
			'nombre_campo'=>'hora_salida', 'tipo_dato' => 'text', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$area = array(
			'nombre_campo'=>'area', 'tipo_dato' => 'text', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
		$personal['nombre_tabla'] = 'personal';
		$personal['campos'] = array(
			'id' => $id, 'nombre' => $nombre, 'apellido_paterno' => $apellido_paterno, 'apellido_materno' => $apellido_materno, 'fecha_nacimiento' => $fecha_nacimiento, 'foto' => $foto, 'observaciones' => $observaciones,'telefono' => $telefono,'puesto' =>$puesto, 'status' => $status, 'codigo_barras' => $codigo_barras, 'domicilio' => $domicilio, 'hora_entrada' => $hora_entrada, 'area' => $area, 'hora_salida' => $hora_salida);

		$madre_padre_tutor_id = array(
			'nombre_campo'=>'madre_padre_tutor_id', 'tipo_dato' => 'int(11)', 'nulo' => 'NOT NULL', 'primary' => false,'autoincrement' => false);
		$persona_autorizada_id = array(
			'nombre_campo'=>'persona_autorizada_id', 'tipo_dato' => 'int(11)', 'nulo' => 'NOT NULL', 'primary' => false,'autoincrement' => false);
		$alumno_id = array(
			'nombre_campo'=>'alumno_id', 'tipo_dato' => 'int(11)', 'nulo' => 'NOT NULL', 'primary' => false,'autoincrement' => false);

	    $asigna_madre_padre_tutor['nombre_tabla'] = 'asigna_madre_padre_tutor';
		$asigna_madre_padre_tutor['campos'] = array(
			'id'=>$id, 'alumno_id'=>$alumno_id, 'madre_padre_tutor_id'=>$madre_padre_tutor_id);

		$asigna_persona_autorizada['nombre_tabla'] = 'asigna_persona_autorizada';
		$asigna_persona_autorizada['campos'] = array(
			'id'=>$id, 'alumno_id'=>$alumno_id, 'persona_autorizada_id'=>$persona_autorizada_id);

		$alumno_id_registro = array(
			'nombre_campo'=>'alumno_id', 'tipo_dato' => 'int(11)', 'nulo' => false, 'primary' => false,'autoincrement' => false);
		$madre_padre_tutor_id_registro = array(
			'nombre_campo'=>'madre_padre_tutor_id', 'tipo_dato' => 'int(11)', 'nulo' => false, 'primary' => false,'autoincrement' => false);
		$persona_autorizada_id_registro = array(
			'nombre_campo'=>'persona_autorizada_id', 'tipo_dato' => 'int(11)', 'nulo' => false, 'primary' => false,'autoincrement' => false);
    	$personal_id_registro = array(
			'nombre_campo'=>'personal_id', 'tipo_dato' => 'int(11)', 'nulo' => false, 'primary' => false,'autoincrement' => false);
    	$fecha = array(
			'nombre_campo'=>'fecha', 'tipo_dato' => 'DATE', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);
    	$hora = array(
			'nombre_campo'=>'hora', 'tipo_dato' => 'DATETIME', 'nulo' => 'NOT NULL', 'primary' => false, 'autoincrement' => false);

		$registro['nombre_tabla'] = 'registro';
		$registro['campos'] = array(
			'id'=>$id, 
			'codigo_barras'=>$codigo_barras,
			'alumno_id' => $alumno_id_registro,
			'madre_padre_tutor_id' => $madre_padre_tutor_id_registro,
		    'persona_autorizada_id' => $persona_autorizada_id_registro,
		    'personal_id' => $personal_id_registro,
			'fecha'=>$fecha,
			'hora'=>$hora
			);
		/*
		$registro['campos'] = array(
			'id'=>$id, 
			'codigo_barras'=>$codigo_barras,
			'alumno_id' => $alumno_id_registro,
		    'madre_padre_tutor_id' => $madre_padre_tutor_id_registro,
		    'persona_autorizada_id' => $persona_autorizada_id_registro,
		    'personal_id' => $personal_id_registro,
			'fecha'=>$fecha,
			'hora'=>$hora
			);
		*/

		$database = array('grupo'=>$grupo,'seccion'=>$seccion,'usuario'=>$usuario,'accion'=>$accion,'accion_grupo'=>$accion_grupo, 'madre_padre_tutor' => $madre_padre_tutor, 'persona_autorizada'=>$persona_autorizada, 'token' => $token, 'alumno' => $alumno, 'personal' => $personal,'asigna_madre_padre_tutor' => $asigna_madre_padre_tutor,'asigna_persona_autorizada' => $asigna_persona_autorizada,'registro' => $registro);

		return $database;
	}


//---------------------------------------------------------------
	public function genera_lista_accion(){
        $conexion = new Conexion();
        $conexion->selecciona_base_datos();
        $link = $conexion->link;
        $result = $link->query("SELECT a.id,a.descripcion,s.descripcion as descripcion_seccion FROM accion a, seccion s WHERE s.id=a.seccion_id");

        while( $row = mysqli_fetch_assoc( $result)){
		    $new_array[] = $row; // Inside while loop
		}
        if (!empty($new_array)) {
			return $new_array;
		}

    } 

    public function genera_lista_asigna_persona_autorizada(){
    	$conexion = new Conexion();
        $conexion->selecciona_base_datos();
        $link = $conexion->link;
        $result = $link->query("SELECT apa.id,CONCAT(pa.nombre,' ',pa.apellido_paterno,' ',pa.apellido_materno) AS nombre_persona_autorizada,CONCAT(a.nombre,' ',a.apellido_paterno,' ',a.apellido_materno) AS nombre_alumno,apa.persona_autorizada_id FROM asigna_persona_autorizada apa, alumno a,persona_autorizada pa WHERE a.id=apa.alumno_id");

        while( $row = mysqli_fetch_assoc( $result)){
		    $new_array[] = $row; // Inside while loop
		}
		if (!empty($new_array)) {
			return $new_array;
		}
    }
    public function genera_lista_asigna_madre_padre_tutor(){
        $conexion = new Conexion();
        $conexion->selecciona_base_datos();
        $link = $conexion->link;
        $result = $link->query("SELECT ampt.id,CONCAT(mpt.nombre,' ',mpt.apellido_paterno,' ',mpt.apellido_materno) AS nombre_madre_padre_tutor,CONCAT(a.nombre,' ',a.apellido_paterno,' ',a.apellido_materno) AS nombre_alumno,ampt.madre_padre_tutor_id FROM asigna_madre_padre_tutor ampt, alumno a,madre_padre_tutor mpt WHERE a.id=ampt.alumno_id");

        while( $row = mysqli_fetch_assoc( $result)){
		    $new_array[] = $row; // Inside while loop
		}
		if (!empty($new_array)) {
			return $new_array;
		}
		
    }
    public function genera_lista_accion_grupo(){
        $conexion = new Conexion();
        $conexion->selecciona_base_datos();
        $link = $conexion->link;
        $result = $link->query("SELECT ag.id,a.descripcion,g.descripcion AS descripcion_grupo FROM accion_grupo ag,accion a,grupo g
        						WHERE (ag.accion_id=a.id) AND (ag.grupo_id=g.id)");

        while( $row = mysqli_fetch_assoc( $result)){
		    $new_array[] = $row; // Inside while loop
		}
		if (!empty($new_array)) {
			return $new_array;
		}
		
    }
	public function genera_lista_grupo(){
        $conexion = new Conexion();
        $conexion->selecciona_base_datos();
        $link = $conexion->link;
        $result = $link->query("SELECT id,descripcion,status FROM grupo");

        while( $row = mysqli_fetch_assoc( $result)){
		    $new_array[] = $row; // Inside while loop
		}
		if (!empty($new_array)) {
			return $new_array;
		}
    } 

     public function genera_lista_madre_padre_tutor(){
        $conexion = new Conexion();
        $conexion->selecciona_base_datos();
        $link = $conexion->link;
        $result = $link->query("SELECT id,nombre,apellido_paterno,apellido_materno,status,foto,telefono,codigo_barras FROM madre_padre_tutor");

        while( $row = mysqli_fetch_assoc( $result)){
		    $new_array[] = $row; // Inside while loop
		}
		if (!empty($new_array)) {
			return $new_array;
		}
	}

	public function genera_lista_persona_autorizada(){
        $conexion = new Conexion();
        $conexion->selecciona_base_datos();
        $link = $conexion->link;
        $result = $link->query("SELECT id,nombre,apellido_paterno,apellido_materno,status,foto,telefono,codigo_barras FROM persona_autorizada");

        while( $row = mysqli_fetch_assoc( $result)){
		    $new_array[] = $row; // Inside while loop
		}
		if (!empty($new_array)) {
			return $new_array;
		}
	}
	public function genera_lista_alumno(){
        $conexion = new Conexion();
        $conexion->selecciona_base_datos();
        $link = $conexion->link;
        $result = $link->query("SELECT id,nombre,apellido_paterno,apellido_materno,status,foto,codigo_barras FROM alumno");

        while( $row = mysqli_fetch_assoc( $result)){
		    $new_array[] = $row; // Inside while loop
		}
		if (!empty($new_array)) {
			return $new_array;
		}
	}

	public function genera_lista_personal(){
        $conexion = new Conexion();
        $conexion->selecciona_base_datos();
        $link = $conexion->link;
        $result = $link->query("SELECT id,nombre,apellido_paterno,apellido_materno,status,foto,telefono,puesto,codigo_barras FROM personal");

        while( $row = mysqli_fetch_assoc( $result)){
		    $new_array[] = $row; // Inside while loop
		}
		if (!empty($new_array)) {
			return $new_array;
		}
	}

	public function genera_lista_token(){
        $conexion = new Conexion();
        $conexion->selecciona_base_datos();
        $link = $conexion->link;
        $result = $link->query("SELECT t.id,t.nombre_usuario,t.token,g.descripcion FROM token t, grupo g WHERE t.grupo_id = g.id");

        while( $row = mysqli_fetch_assoc( $result)){
		    $new_array[] = $row; // Inside while loop
		}
		if (!empty($new_array)) {
			return $new_array;
		}
	}


    public function genera_lista_seccion(){
        $conexion = new Conexion();
        $conexion->selecciona_base_datos();
        $link = $conexion->link;
        $result = $link->query("SELECT id,descripcion,status FROM seccion");

        while( $row = mysqli_fetch_assoc( $result)){
		    $new_array[] = $row; // Inside while loop
		}
		if (!empty($new_array)) {
			return $new_array;
		}

    } 
    public function genera_lista_usuario(){
        $conexion = new Conexion();
        $conexion->selecciona_base_datos();
        $link = $conexion->link;
        $result = $link->query("SELECT u.id,u.user,u.email,g.descripcion FROM usuario u,grupo g WHERE g.id=u.grupo_id");

        while( $row = mysqli_fetch_assoc( $result)){
		    $new_array[] = $row; // Inside while loop
		}
        if (!empty($new_array)) {
			return $new_array;
		}
    } 

    public function genera_lista_session(){
        $conexion = new Conexion();
        $conexion->selecciona_base_datos();
        $link = $conexion->link;
        $result = $link->query("SELECT u.id,u.user,u.password,u.email,g.id AS grupo_id, g.descripcion FROM usuario u,grupo g WHERE g.id=u.grupo_id");

        while( $row = mysqli_fetch_assoc( $result)){
		    $new_array[] = $row; // Inside while loop
		}
        if (!empty($new_array)) {
			return $new_array;
		}
    } 
         
//------------------------------------------------------------------



	public function genera_valor_insercion($campo, $registro, $valores, $i, $n_campos){
		if(!array_key_exists($campo, $registro)){
			if($campo == "id"){
				$valores = $valores."null";	
			}
			else{
				$valores = $valores."''";
			}
		}
		else{
			$valores = $valores."'".$registro[$campo]."'";
		}
		if($i<$n_campos){
			$valores = $valores.",";
		}
		return $valores;
	}

	public function genera_valores_insercion($tabla_base,$registro){
		$valores = "";
		$n_campos = count($tabla_base);
		$i = 1;
		foreach ($tabla_base as $campo => $atributos) {
			$valores = $this->genera_valor_insercion($campo, $registro, $valores, $i, $n_campos);
			$i++;
		}
		return $valores;
	}

	public function modifica_db($registro,$tabla,$nombre_base_datos){
	
		if(is_array($registro)){
			$conexion = new Conexion();
			$conexion->selecciona_base_datos($nombre_base_datos);
			$link = $conexion->link;
		    $estructura = $this->genera_estructura();
		    
		    if(!array_key_exists($tabla, $estructura)){ //errro validado
		    	$this->error(32,__LINE__,__FILE__);
		    }
		    else{
		    	$tabla_base = $estructura[$tabla]['campos'];
		    	if(!$this->valida_campos_nulos($registro, $tabla_base)){
		    		$this->error(37,__LINE__,__FILE__);
		    	}
		    	if(!$this->valida_campos_obligatorios($tabla_base, $registro)){
		    		$this->error(38,__LINE__,__FILE__);
		    	}
		    }
		}
		else{ // error validado
			$this->error(24,__LINE__,__FILE__);
		}
		
		$campos = "";
		$n_campos = count($tabla_base);
		$i = 0;
		foreach ($tabla_base as $campo => $atributos) {
			if($campo !== 'id'){
				if($i<$n_campos){
					$campos.=$campos==""?"$campo='$registro[$campo]'":",$campo='$registro[$campo]'";
				}
			}else{
				$valor_id = $registro[$campo];
				$campo_id = $campo."=".$registro[$campo];
			}

			$i++;
		}

		$consulta_modifica = "UPDATE $tabla SET $campos WHERE $campo_id";

		$link->query($consulta_modifica);
		
		return $valor_id;
	}

	public function modifica_codigo_barras($registro,$tabla,$id,$nombre_base_datos=false){
	
		if(!empty($registro)){
			$conexion = new Conexion();
			$conexion->selecciona_base_datos($nombre_base_datos);
			$link = $conexion->link;
		}
		else{ // error validado
			$this->error(24,__LINE__,__FILE__);
		}

		$consulta_insercion = "UPDATE $tabla SET codigo_barras='$registro' WHERE id=$id";
		$link->query($consulta_insercion);
		if($link->error){
			$this->error(36,__LINE__,__FILE__);
			return false;
		}
		else{
			return $id;
		}

	}	

//SELECT mpt.foto FROM madre_padre_tutor mpt, alumno a, asigna_madre_padre_tutor ampt WHERE ampt.alumno_id = a.id && ampt.madre_padre_tutor_id = mpt.id;
	public function obten_por_codigo_barras_fecha($codigo_barras=False,$condicion=False, $nombre_base_datos=False){ //finalizado
		
		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);
		$link = $conexion->link;
		$modelo = new Modelos();
		
		$database = $modelo->genera_estructura();
		if(!array_key_exists('registro', $database)){
			$this->error(32,__LINE__,__FILE__);
			return false;
		}

		$tabla_base = $database['registro']['campos'];
		$consulta = "SELECT * FROM registro WHERE codigo_barras ='".$codigo_barras."' && fecha = CURDATE() && alumno_id ".$condicion." ORDER BY hora";
		$result = $link->query($consulta);
		if($link->error){
			$this->error(39,__LINE__,__FILE__);
			return false;
		}
		$resultado_envio=array();
		$i=0;
		while($row = $result->fetch_array()){
			foreach ($tabla_base as $campo => $valor) {
				$resultado_envio[$i][$campo] = $row[$campo];
			}
			$i++;
		}

		return $resultado_envio;

	}
	public function comprueba_responsable($tabla=False,$alumno_id=False, $codigo_barras_alumno=False,$responsable_id=False,$codigo_barras_responsable=False, $nombre_base_datos=False){ //finalizado
		
		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);
		$link = $conexion->link;
		$modelo = new Modelos();
		
		$database = $modelo->genera_estructura();
		if(!array_key_exists($tabla, $database)){
			$this->error(32,__LINE__,__FILE__);
			return false;
		}

		$tabla_base = $database['alumno']['campos'];
		$consulta = "SELECT a.*,CONCAT('./views/alumno/fotos/',a.foto) AS foto FROM alumno a, ".$tabla." t,asigna_".$tabla." at WHERE a.codigo_barras = '".$codigo_barras_alumno."' && t.codigo_barras = '".$codigo_barras_responsable."' && at.alumno_id = '".$alumno_id."' && at.".$tabla."_id = '".$responsable_id."'";
		
		/*
		"SELECT t.nombre,t.foto FROM alumno a, ".$tabla." t,asigna_".$tabla." at WHERE a.codigo_barras = '".$codigo_barras_alumno."' && t.codigo_barras = '".$codigo_barras_responsable."' && at.alumno_id = '".$alumno_id."' && at.madre_padre_tutor_id = '".$responsable_id."'"
		*/

		$result = $link->query($consulta);
		//print_r($result);
		if($link->error){
			$this->error(39,__LINE__,__FILE__);
			return false;
		}
		$resultado_envio=array();
		$i=0;
		while($row = $result->fetch_array()){
			foreach ($tabla_base as $campo => $valor) {
				$resultado_envio[$i][$campo] = $row[$campo];
			}
			$i++;
		}

		return $resultado_envio;

	}
	public function obten_fotos($tabla=False, $alumno_id=False, $nombre_base_datos=False){ //finalizado
		
		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);
		$link = $conexion->link;
		$modelo = new Modelos();
		
		$database = $modelo->genera_estructura();
		if(!array_key_exists($tabla, $database)){
			$this->error(32,__LINE__,__FILE__);
			return false;
		}

		$tabla_base = $database[$tabla]['campos'];
		$consulta = "SELECT t.*,CONCAT('./views/".$tabla."/fotos/',t.foto) AS foto FROM ".$tabla." t, alumno a, asigna_".$tabla." at WHERE at.alumno_id = '".$alumno_id."' && at.".$tabla."_id = t.id GROUP BY t.id";
		$result = $link->query($consulta);
		
		if($link->error){
			$this->error(39,__LINE__,__FILE__);
			return false;
		}
		$resultado_envio=array();
		$i=0;
		while($row = $result->fetch_array()){
			foreach ($tabla_base as $campo => $valor) {
				$resultado_envio[$i][$campo] = $row[$campo];
			}
			$i++;
		}

		return $resultado_envio;

	}
	public function obten_registro_por_fechas($fecha_inicial=False, $fecha_final=False, $nombre_base_datos=False){ //finalizado
		
		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);
		$link = $conexion->link;
		$modelo = new Modelos();
		
		$database = $modelo->genera_estructura();
		if(!array_key_exists('registro', $database)){
			$this->error(32,__LINE__,__FILE__);
			return false;
		}

		$tabla_base = $database['registro']['campos'];
		$consulta = "SELECT * FROM registro WHERE fecha >= '".$fecha_inicial."' && fecha <= '".$fecha_final."'";
		$result = $link->query($consulta);
		
		if($link->error){
			$this->error(39,__LINE__,__FILE__);
			return false;
		}
		$resultado_envio=array();
		$i=0;
		while($row = $result->fetch_array()){
			foreach ($tabla_base as $campo => $valor) {
				$resultado_envio[$i][$campo] = $row[$campo];
			}
			$i++;
		}

		return $resultado_envio;

	}
	public function obten_por_codigo_barras($tabla=False, $codigo_barras=False, $nombre_base_datos=False){ //finalizado
		
		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);
		$link = $conexion->link;
		$modelo = new Modelos();
		
		$database = $modelo->genera_estructura();
		if(!array_key_exists($tabla, $database)){
			$this->error(32,__LINE__,__FILE__);
			return false;
		}

		$tabla_base = $database[$tabla]['campos'];
		$consulta = "SELECT *FROM ".$tabla. " WHERE codigo_barras = '".$codigo_barras."'";
		$result = $link->query($consulta);

		if($link->error){
			$this->error(39,__LINE__,__FILE__);
			return false;
		}
		$resultado_envio=array();
		$i=0;
		while($row = $result->fetch_array()){
			foreach ($tabla_base as $campo => $valor) {
				$resultado_envio[$i][$campo] = $row[$campo];
			}
			$i++;
		}

		return $resultado_envio;

	}


	public function obten_por_id($tabla=False, $id=False, $nombre_base_datos=False){ //finalizado
		
		$conexion = new Conexion();
		$conexion->selecciona_base_datos($nombre_base_datos);
		$link = $conexion->link;
		$modelo = new Modelos();
		
		$database = $modelo->genera_estructura();
		if(!array_key_exists($tabla, $database)){
			$this->error(32,__LINE__,__FILE__);
			return false;
		}

		$tabla_base = $database[$tabla]['campos'];
		$consulta = "SELECT *FROM ".$tabla. " WHERE id = ".$id;
		$result = $link->query($consulta);

		if($link->error){
			$this->error(39,__LINE__,__FILE__);
			return false;
		}
		$resultado_envio=array();
		$i=0;
		while($row = $result->fetch_array()){
			foreach ($tabla_base as $campo => $valor) {
				$resultado_envio[$i][$campo] = $row[$campo];
			}
			$i++;
		}

		return $resultado_envio;

	}

	public function valida_campo_nulo($tabla_base, $campo, $valor){
		$no_nulo = $tabla_base[$campo]['nulo'];
    	if($no_nulo){
    		if(!$valor){
				return false;
	    	}
	    	else{
	    		return true;
	    	}
	    }
	    return true;		
	}

	public function valida_campo_obligatorio($valor, $campo, $registro){
		if($valor){
    		if($campo != 'id'){
		    	if(!array_key_exists($campo, $registro)){
		    		return false;
		    	}
		    	else{
		    		return true;
		    	}
			}
		}
		return true;
	}

	public function valida_campos_nulos($registro, $tabla_base){
		foreach ($registro as $campo => $valor) {
    		if(!array_key_exists($campo, $tabla_base)){
		    	$this->error(33,__LINE__,__FILE__);
		    	return false;
		   	}
	 		else{
	    		if(!$this->valida_campo_nulo($tabla_base, $campo, $valor)){
		    		$this->error(34,__LINE__,__FILE__);
		    		return false;	    				
		    	}
		    }
		}
		return true;
	}

	public function valida_campos_obligatorios($tabla_base, $registro){
		foreach ($tabla_base as $campo => $atributos) {	
    		foreach ($atributos as $atributo => $valor) {
		    	if(!$this->valida_campo_obligatorio($valor, $campo, $registro)){
					$this->error(35,__LINE__,__FILE__);
		    		return false;		    				
		    	}
		    }
		}
		return true;		
	}
}
?>
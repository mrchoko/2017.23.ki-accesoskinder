<?php

require_once('controlador_base.php');

if(file_exists('./config/conexion.php')){
	require_once('./config/conexion.php');
}
if(file_exists('./config/conexion.php')){
	require_once('./modelos.php');
}
class Controlador_Personal extends Controlador_Base{
	public function lista_personal(){

		$conexion = new Conexion();
		$conexion->selecciona_base_datos();
		$modelo = new modelos();
		$registro_obtenido = $modelo->genera_lista_personal();
		$registro_enviar = $registro_obtenido;
		return $registro_enviar;
	}

}

$personal_controller = new Controlador_Personal();

if( ($accion == 'lista' || $accion == 'credencial_general') && 
	($seccion == 'personal' || $seccion == 'credencial')){
	$personal = $personal_controller->lista_personal();
}

if($accion == 'desactiva' && $seccion == 'personal' ){
	$personal_id = $_GET['personal_id'];
	$personal = $personal_controller->desactiva($personal_id,'personal');
	if($personal){
		header('Location: index.php?seccion=personal&accion=lista&resultado=correcto&operacion=Desactiva');
	}
	else{
		header('Location: index.php?seccion=personal&accion=lista&resultado=incorrecto&operacion=Desactiva');	
	}		
}

if($accion == 'activa' && $seccion == 'personal'){
	$personal_id = $_GET['personal_id'];
	$personal = $personal_controller->activa($personal_id,'personal');
	if($personal){
		header('Location: index.php?seccion=personal&accion=lista&resultado=correcto&operacion=Activa');
	}
	else{
		header('Location: index.php?seccion=personal&accion=lista&resultado=incorrecto&operacion=Activa');	
	}
}

if(isset($_GET['codigo'])){
	if ($accion == 'modifica' && $seccion == 'personal' && !empty($_GET['personal_id'])) {

		$tabla = $_GET['seccion'];
		$id = $_GET['personal_id'];
		$nombre_codigo_barras = pathinfo($_GET['nombre_codigo'],PATHINFO_FILENAME);

		$directorio_codigo_barras = dirname(__DIR__).'/views/'.$tabla.'/codigo_barras';
		unlink($directorio_codigo_barras.'/'.$nombre_codigo_barras.'.jpg');

		$registro = $personal_controller->genera_imagen_codigo_barras($tabla,$nombre_codigo_barras);

		$personal_controller->modifica_codigo_barras($registro,$tabla,$id);

		header('Location:index.php?seccion=personal&accion=modifica&personal_id='.$id);
		
	}
}

if($accion == 'modifica' && $seccion == 'personal'){
	$personal_id = $_GET['personal_id'];

	$conexion = new Conexion();
	$conexion->selecciona_base_datos();

	$modelo = new Modelos();

	$personal = $modelo->obten_por_id('personal',$personal_id );

}

if($accion == 'modifica_bd' && $seccion == 'personal'){

	$personal_id = $_GET['personal_id'];
	$nombre = $_POST['nombre'];
	$apellido_paterno = $_POST['apellido_paterno'];
	$apellido_materno = $_POST['apellido_materno'];
	$fecha_nacimiento = $_POST['fecha_nacimiento'];
	
	$directorio_foto = dirname(__DIR__).'/views/personal/fotos';
	$directorio_codigo_barras = dirname(__DIR__).'/views/personal/codigo_barras';
	if(empty($_FILES['foto']['tmp_name'])){

		$name_file = $_POST['foto_anterior'];

		$name_codigo_barras = pathinfo($name_file,PATHINFO_FILENAME);
		$extension = pathinfo($name_file, PATHINFO_EXTENSION);

		$nombre_archivo=$_POST['nombre'].''.$_POST['apellido_paterno'].''.$_POST['apellido_materno'].'.'.$extension;
		$nombre_archivo_codigo_barras = $_POST['nombre'].''.$_POST['apellido_paterno'].''.$_POST['apellido_materno'].'.jpg';

		rename($directorio_foto.'/'.$name_file, $directorio_foto.'/'.$nombre_archivo);
		rename($directorio_codigo_barras.'/'.$name_codigo_barras.'.jpg', $directorio_codigo_barras.'/'.$nombre_archivo_codigo_barras);

		$foto = $nombre_archivo;
	}
	else{
		$delete_file = $_POST['foto_anterior'];
		unlink($directorio_foto.'/'.$delete_file);

		$archivo=$_FILES['foto']['tmp_name'];
		$name_file = $_FILES['foto']['name'];
		
		$extension = pathinfo($name_file, PATHINFO_EXTENSION);
		$nombre_archivo=$_POST['nombre'].''.$_POST['apellido_paterno'].''.$_POST['apellido_materno'].'.'.$extension;
		move_uploaded_file($archivo, $directorio_foto."/".$nombre_archivo);


		$name_codigo_barras = pathinfo($_POST['foto_anterior'],PATHINFO_FILENAME);
		$nombre_archivo_codigo_barras = $_POST['nombre'].''.$_POST['apellido_paterno'].''.$_POST['apellido_materno'].'.jpg';
		rename($directorio_codigo_barras.'/'.$name_codigo_barras.'.jpg', $directorio_codigo_barras.'/'.$nombre_archivo_codigo_barras);

		$nombre_archivo=$_POST['nombre'].''.$_POST['apellido_paterno'].''.$_POST['apellido_materno'].'.'.$extension;

		$foto = $nombre_archivo;
	}

	$observaciones = $_POST['observaciones'];
	$status=$_POST['status'];
	$telefono = $_POST['telefono'];
	$puesto = $_POST['puesto'];
	$codigo_barras = $_POST['codigo_barras'];
	$domicilio = $_POST['domicilio'];
	$hora_entrada = $_POST['hora_entrada'];
	$area = $_POST['area'];
	$hora_salida = $_POST['hora_salida'];

	$registro = array(
		'id'=>$personal_id,'nombre'=>$nombre, 'apellido_paterno'=>$apellido_paterno, 'apellido_materno'=>$apellido_materno,'fecha_nacimiento'=>$fecha_nacimiento,'foto'=>$foto, 'observaciones'=>$observaciones, 'status'=>$status,'telefono' => $telefono,'puesto' => $puesto, 'codigo_barras'=>$codigo_barras, 'domicilio'=>$domicilio, 'hora_entrada'=>$hora_entrada, 'area'=>$area, 'hora_salida'=>$hora_salida);
	$tabla = 'personal';
	$personal1 = $personal_controller->modifica($registro,$tabla);
	if($personal1){
		$personal_controller->redirect($registro, 'lista', 'personal','modifica');
	}
	else{
		$personal_controller->redirect($registro, 'lista', 'personal','modifica');
	}

}
?>
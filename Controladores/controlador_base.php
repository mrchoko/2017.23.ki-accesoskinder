<?php

require(dirname(__DIR__)."/Controladores/lib/BarcodeGenerator.php");

require_once(dirname(__DIR__)."/Controladores/lib/BarcodeGeneratorJPG.php");

if(file_exists('../config/conexion.php')){
	require_once('../config/conexion.php');
}
if(file_exists('../modelos.php')){
	require_once('../modelos.php');
}


class Controlador_Base{

	public function activa($id=False,$tabla=False,$nombre_base_datos=False){
		$now = time();
		if($now > $_SESSION['expire']) {
			session_destroy();
			header('Location: views/session/');
		}
		else{
			$conexion = new Conexion();
			$conexion->selecciona_base_datos($nombre_base_datos);

			$modelo = new Modelos();
			$resultado = $modelo->activa_db($id,$tabla);

			return $resultado;
		}
	}

	public function desactiva($id,$tabla,$nombre_base_datos){
		$now = time();
		if($now > $_SESSION['expire']) {
			session_destroy();
			header('Location: views/session/');
		}
		else{
			$conexion = new Conexion();
			$conexion->selecciona_base_datos($nombre_base_datos);

			$modelo = new Modelos();
			$resultado = $modelo->desactiva_db($id,$tabla);

			return $resultado;
		}
	}

	public function redirect($registro, $accion, $tabla,$operacion,$valor=false){

		if($registro){
			header('Location: index.php?seccion='.$tabla.'&accion='.$accion.'&resultado=correcto&operacion='.$operacion.'&valor='.$valor);
			exit;
		}
		else{
			header('Location: index.php?seccion='.$tabla.'&accion='.$accion.'&resultado=incorrecto&operacion='.$operacion.'&valor='.$valor);
			exit;
		}
	}

	public function desactiva_db(){
		$now = time();
		if($now > $_SESSION['expire']) {
			session_destroy();
			header('Location: views/session/');
		}
		else{
			$tabla = $_GET['seccion'];
			$registro_id = $_GET[$tabla.'_id'];

			$registro = $this->desactiva($registro_id, $tabla);		
		}
	}	

	public function elimina($id,$tabla){
		$now = time();
		if($now > $_SESSION['expire']) {
			session_destroy();
			header('Location: views/session/');
		}
		else{
			$conexion = new Conexion();
			$conexion->selecciona_base_datos();

			$modelo = new Modelos();
			$modelo->elimina_db($tabla,$id);
			$registro_obtenido = $modelo->obten_por_id($tabla,$id);
			
			if (!$registro_obtenido) {
				return true;
			}
			else{
				return false;
			}
		}
	}

	public function elimina_bd(){
		$valor_filtro = $_GET['valor'];

		echo $valor_filtro;

		$now = time();
		if($now > $_SESSION['expire']) {
			session_destroy();
			header('Location: views/session/');
		}
		else{
			$tabla = $_GET['seccion'];
			$registro_id = $_GET[$tabla.'_id'];

			$registro = $this->elimina($registro_id,$tabla);

			if ($registro) {
				if ($tabla === 'persona_autorizada' || $tabla === 'madre_padre_tutor' || $tabla === 'alumno' || $tabla === 'personal') {
					$directorio_foto = dirname(__DIR__).'/views/'.$tabla.'/fotos';
					$directorio_codigo_barras = dirname(__DIR__).'/views/'.$tabla.'/codigo_barras';
					$delete_foto = $_GET['foto'];
					$delete_codigo_barras = $_GET['codigo_barras'];
					unlink($directorio_foto.'/'.$delete_foto);
					unlink($directorio_codigo_barras.'/'.$delete_codigo_barras);
				}
			}

			$this->redirect($registro, 'lista', $tabla,'Elimina', $valor_filtro);
		}
	}	

	public function inserta($registro,$tabla,$nombre_base_datos=false){
		$now = time();
		if($now > $_SESSION['expire']) {
			session_destroy();
			header('Location: views/session/');
		}
		else{
			$conexion = new Conexion();
			$conexion->selecciona_base_datos($nombre_base_datos);

			$modelo = new Modelos();
			$registro_id = $modelo->alta_db($registro,$tabla,$nombre_base_datos);
			if($registro_id){
				$registro_obtenido = $modelo->obten_por_id($tabla,$registro_id,$nombre_base_datos);
				$registro_enviar[$tabla] = $registro_obtenido;
				return $registro_enviar;
			}
			else{
				return false;
			}
		}
	}
	public function modifica($registro,$tabla,$nombre_base_datos){
		$now = time();
		if($now > $_SESSION['expire']) {
			session_destroy();
			header('Location: views/session/');
		}
		else{
			$conexion = new Conexion();
			$conexion->selecciona_base_datos($nombre_base_datos);

			$modelo = new Modelos();
			$registro_id = $modelo->modifica_db($registro,$tabla);

			if($registro_id){
				$registro_obtenido = $modelo->obten_por_id($tabla,$registro_id,$nombre_base_datos);
				$registro_enviar[$tabla] = $registro_obtenido;
				return $registro_enviar;
			}
			else{
				return false;
			}
		}
	}

	public function alta_bd(){
		$now = time();
		if($now > $_SESSION['expire']) {
			session_destroy();
			header('Location: views/session/');
		}
		else{
			$campos = $this->obten_estructura($_GET['seccion']);
			$registro = $this->genera_registro_alta($campos);
			$tabla = $this->inserta($registro,$_GET['seccion']);
			print_r($registro);
			$this->redirect($tabla, 'lista', $_GET['seccion'],'Guarda');
		}
	}

	public function genera_registro_alta($campos_grupo){
		foreach ($campos_grupo as $key => $campo) {
			if($key!='id'){
				if($key=='status'){
					$registro[$key] = (isset($_POST['status'])) ? 1 : 0;
				}elseif ($key=='foto') {
					
					$directorio = dirname(__DIR__).'/views/'.$_GET['seccion'].'/fotos';
					$archivo=$_FILES['foto']['tmp_name'];
					//$nombrearchivo=$_FILES['foto']['name']; 
					$name_file = $_FILES['foto']['name'];
					$extension = pathinfo($name_file, PATHINFO_EXTENSION);
					$nombre_archivo=$_POST['nombre'].''.$_POST['apellido_paterno'].''.$_POST['apellido_materno'].'.'.$extension;
					move_uploaded_file($archivo, $directorio."/".$nombre_archivo);

					$registro[$key] = $nombre_archivo;

				}elseif ($key=='codigo_barras') {
									
					$nombre_archivo=$_POST['nombre'].''.$_POST['apellido_paterno'].''.$_POST['apellido_materno'];

					$registro[$key] = $this->genera_imagen_codigo_barras($_GET['seccion'],$nombre_archivo);
					
				}
				else{
					$registro[$key] = $_POST[$key];	
				}
			}
		}
		return $registro;
	}

	public function obten_estructura($tabla){
		$modelo = new modelos();
		$db = $modelo->genera_estructura();
		$estructura_grupo = $db[$tabla];
		$campos_grupo = $estructura_grupo['campos'];
		return $campos_grupo;
	}

	public function modifica_codigo_barras($registro,$tabla,$id,$nombre_base_datos=false){
		$modelo = new modelos();
		$id = $modelo->modifica_codigo_barras($registro,$tabla,$id,$nombre_base_datos);
		return $id;

	}

	public function genera_imagen_codigo_barras($tabla,$nombre_codigo_barras){
		$generator = new BarcodeGeneratorJPG();					

		$directorio = dirname(__DIR__).'/views/'.$tabla.'/codigo_barras/';
		$nombre_archivo=$nombre_codigo_barras.'.jpg';

		if($tabla == 'madre_padre_tutor'){
			$clave = uniqid('MPT_');

			$barcode = $generator->getBarcode($clave, $generator::TYPE_CODE_128);
			file_put_contents($directorio.''.$nombre_archivo, $barcode);
		}
		if($tabla == 'persona_autorizada'){
			$clave = uniqid('PA_');

			$barcode = $generator->getBarcode($clave, $generator::TYPE_CODE_128);
			file_put_contents($directorio.''.$nombre_archivo, $barcode);
		}
		if($tabla == 'alumno'){
			$clave = uniqid('A_');

			$barcode = $generator->getBarcode($clave, $generator::TYPE_CODE_128);
			file_put_contents($directorio.''.$nombre_archivo, $barcode);
		}
		if($tabla == 'personal'){
			$clave = uniqid('P_');

			$barcode = $generator->getBarcode($clave, $generator::TYPE_CODE_128);
			file_put_contents($directorio.''.$nombre_archivo, $barcode);
		}
		return $clave;
	}
}
?>
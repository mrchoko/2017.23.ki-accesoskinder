<?php
require_once('controlador_base.php');
if(file_exists('./config/conexion.php')){
	require_once('./config/conexion.php');
}
if(file_exists('./config/conexion.php')){
	require_once('./modelos.php');
}
class Controlador_Seccion extends Controlador_Base{
	public function lista_seccion(){

		$conexion = new Conexion();
		$conexion->selecciona_base_datos();
		$modelo = new modelos();
		$registro_obtenido = $modelo->genera_lista_seccion();
		$registro_enviar = $registro_obtenido;
		return $registro_enviar;
	}
}

$seccion_controller = new Controlador_Seccion();


if($accion == 'lista' && $seccion == 'seccion'){
	$secciones = $seccion_controller->lista_seccion();
}


if($accion == 'desactiva' && $seccion == 'seccion'){
	$seccion_id = $_GET['seccion_id'];
	$seccion = $seccion_controller->desactiva($seccion_id,'seccion',$nombre_base_datos);
	if($seccion){
		header('Location: index.php?seccion=seccion&accion=lista&resultado=correcto&operacion=Desactiva');
	}
	else{
		header('Location: index.php?seccion=seccion&accion=lista&resultado=incorrecto&operacion=Desactiva');	
	}		
}

if($accion == 'activa' && $seccion == 'seccion'){
	$seccion_id = $_GET['seccion_id'];
	$seccion = $seccion_controller->activa($seccion_id,'seccion',$nombre_base_datos);
	if($seccion){
		header('Location: index.php?seccion=seccion&accion=lista&resultado=correcto&operacion=Activa');
	}
	else{
		header('Location: index.php?seccion=seccion&accion=lista&resultado=incorrecto&operacion=Activa');	
	}
}

if($accion == 'modifica' && $seccion == 'seccion'){
	$seccion_id = $_GET['seccion_id'];

	$conexion = new Conexion();
	$conexion->selecciona_base_datos($nombre_base_datos);

	$modelo = new Modelos();

	$seccion1 = $modelo->obten_por_id('seccion',$seccion_id ,$nombre_base_datos);

}

if($accion == 'modifica_bd' && $seccion == 'seccion'){
	$seccion_id = $_GET['seccion_id'];
	$descripcion = $_POST['descripcion'];
	$observaciones = $_POST['observaciones'];
	$status=$_POST['status'];

	$registro = array(
		'id'=>$seccion_id,'descripcion'=>$descripcion, 'observaciones'=>$observaciones, 'status'=>$status);
	$tabla = 'seccion';
	$seccion1 = $seccion_controller->modifica($registro,$tabla,$nombre_base_datos);
	if($seccion1){
		header('Location: index.php?seccion=seccion&accion=modifica&resultado=correcto&operacion=Modifica&seccion_id='.$seccion_id);
	}
	else{
		header('Location: index.php?seccion=seccion&accion=modifica&resultado=incorrecto&operacion=Modifica&seccion_id='.$seccion_id);	
	}
}
?>
<?php
require_once('./Controladores/controlador_madre_padre_tutor.php');
require_once('./Controladores/controlador_persona_autorizada.php');
require_once('./Controladores/controlador_alumno.php');
require_once('./Controladores/controlador_personal.php');

if(file_exists('./config/conexion.php')){
	require_once('./config/conexion.php');
}
if(file_exists('./modelos.php')){
	require_once('./modelos.php');
}

class Controlador_Reportes{
	public function lista_registro($fecha_inicial,$fecha_final){

		$conexion = new Conexion();
		$conexion->selecciona_base_datos();
		$modelo = new modelos();
		$registro_obtenido = $modelo->obten_registro_por_fechas($fecha_inicial,$fecha_final);
		$registro_enviar = $registro_obtenido;
		return $registro_enviar;
	}
}

$registro_controller = new Controlador_Reportes();
$madre_padre_tutor_controller = new Controlador_Madre_Padre_Tutor();
$persona_autorizada_controller = new Controlador_Persona_Autorizada();
$alumno_controller = new Controlador_Alumno();
$personal_controller = new Controlador_Personal();
$asigna_responsable_controller = new Modelos();

if( $accion == 'reporte' && $seccion == 'reportes'){
	if(!isset($_POST['fecha_inicial'])){
		$fecha_inicial='';
	}
	else{
		$fecha_inicial = $_POST['fecha_inicial'];
	}
	if(!isset($_POST['fecha_final'])){
		$fecha_final='';
	}
	else{
		$fecha_final = $_POST['fecha_final'];
	}
	//$fecha_inicial = $_POST['fecha_inicial'];
	//$fecha_final = $_POST['fecha_final'];
	$registros = $registro_controller->lista_registro($fecha_inicial,$fecha_final);

	$alumnos = $alumno_controller->lista_alumno();
	$personales = $personal_controller->lista_personal();
	$madres_padres_tutores = $madre_padre_tutor_controller->lista_madre_padre_tutor();
	$personas_autorizadas = $persona_autorizada_controller->lista_persona_autorizada();
	$asigna_madres_padres_tutores = $asigna_responsable_controller->genera_lista_asigna_madre_padre_tutor();
	$asigna_personas_autorizadas = $asigna_responsable_controller->genera_lista_asigna_persona_autorizada();
}
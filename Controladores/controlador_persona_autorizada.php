<?php
require_once('controlador_base.php');

if(file_exists('./config/conexion.php')){
	require_once('./config/conexion.php');
}
if(file_exists('./config/conexion.php')){
	require_once('./modelos.php');
}
class Controlador_Persona_Autorizada extends Controlador_Base{
	public function lista_persona_autorizada(){

		$conexion = new Conexion();
		$conexion->selecciona_base_datos();
		$modelo = new modelos();
		$registro_obtenido = $modelo->genera_lista_persona_autorizada();
		$registro_enviar = $registro_obtenido;
		return $registro_enviar;
	}
}

$persona_autorizada_controller= new Controlador_Persona_Autorizada();

if( ($accion == 'lista' || $accion == 'credencial_general') && 
	($seccion == 'persona_autorizada' || $seccion == 'credencial') ){
	$persona_autorizada = $persona_autorizada_controller->lista_persona_autorizada();
}

if($accion == 'desactiva' && $seccion == 'persona_autorizada' ){
	$persona_autorizada_id = $_GET['persona_autorizada_id'];
	$persona_autorizada = $persona_autorizada_controller->desactiva($persona_autorizada_id,'persona_autorizada');
	if($persona_autorizada){
		header('Location: index.php?seccion=persona_autorizada&accion=lista&resultado=correcto&operacion=Desactiva');
	}
	else{
		header('Location: index.php?seccion=persona_autorizada&accion=lista&resultado=incorrecto&operacion=Desactiva');	
	}		
}

if($accion == 'activa' && $seccion == 'persona_autorizada'){
	$persona_autorizada_id = $_GET['persona_autorizada_id'];
	$persona_autorizada = $persona_autorizada_controller->activa($persona_autorizada_id,'persona_autorizada');
	if($persona_autorizada){
		header('Location: index.php?seccion=persona_autorizada&accion=lista&resultado=correcto&operacion=Activa');
	}
	else{
		header('Location: index.php?seccion=persona_autorizada&accion=lista&resultado=incorrecto&operacion=Activa');	
	}
}


if(isset($_GET['codigo'])){
	if ($accion == 'modifica' && $seccion == 'persona_autorizada' && !empty($_GET['persona_autorizada_id'])) {

		$tabla = $_GET['seccion'];
		$id = $_GET['persona_autorizada_id'];
		$nombre_codigo_barras = pathinfo($_GET['nombre_codigo'],PATHINFO_FILENAME);

		$directorio_codigo_barras = dirname(__DIR__).'/views/'.$tabla.'/codigo_barras';
		unlink($directorio_codigo_barras.'/'.$nombre_codigo_barras.'.jpg');

		$registro = $persona_autorizada_controller->genera_imagen_codigo_barras($tabla,$nombre_codigo_barras);

		$persona_autorizada_controller->modifica_codigo_barras($registro,$tabla,$id);

		header('Location:index.php?seccion=persona_autorizada&accion=modifica&persona_autorizada_id='.$id);
		
	}
}


if($accion == 'modifica' && $seccion == 'persona_autorizada'){
	$persona_autorizada_id = $_GET['persona_autorizada_id'];

	$conexion = new Conexion();
	$conexion->selecciona_base_datos();

	$modelo = new Modelos();

	$persona_autorizada = $modelo->obten_por_id('persona_autorizada',$persona_autorizada_id );

}

if($accion == 'modifica_bd' && $seccion == 'persona_autorizada'){
	$persona_autorizada_id = $_GET['persona_autorizada_id'];
	$nombre = $_POST['nombre'];
	$apellido_paterno = $_POST['apellido_paterno'];
	$apellido_materno = $_POST['apellido_materno'];
	$fecha_nacimiento = $_POST['fecha_nacimiento'];
	
	$directorio_foto = dirname(__DIR__).'/views/persona_autorizada/fotos';
	$directorio_codigo_barras = dirname(__DIR__).'/views/persona_autorizada/codigo_barras';
	if(empty($_FILES['foto']['tmp_name'])){
		
		$name_file = $_POST['foto_anterior'];
		$extension = pathinfo($name_file, PATHINFO_EXTENSION);

		$nombre_archivo=$_POST['nombre'].''.$_POST['apellido_paterno'].''.$_POST['apellido_materno'].'.'.$extension;
		rename($directorio_foto.'/'.$name_file, $directorio_foto.'/'.$nombre_archivo);

		$name_codigo_barras = pathinfo($name_file,PATHINFO_FILENAME);
		$nombre_archivo_codigo_barras = $_POST['nombre'].''.$_POST['apellido_paterno'].''.$_POST['apellido_materno'].'.jpg';
		rename($directorio_codigo_barras.'/'.$name_codigo_barras.'.jpg', $directorio_codigo_barras.'/'.$nombre_archivo_codigo_barras);

		$foto = $nombre_archivo;
	}
	else{
		$delete_file = $_POST['foto_anterior'];
		unlink($directorio_foto.'/'.$delete_file);

		$archivo=$_FILES['foto']['tmp_name'];

		$name_file = $_FILES['foto']['name'];
		$extension = pathinfo($name_file, PATHINFO_EXTENSION);
		$nombre_archivo=$_POST['nombre'].''.$_POST['apellido_paterno'].''.$_POST['apellido_materno'].'.'.$extension;
		move_uploaded_file($archivo, $directorio_foto."/".$nombre_archivo);

		$name_codigo_barras = pathinfo($_POST['foto_anterior'],PATHINFO_FILENAME);
		$nombre_archivo_codigo_barras = $_POST['nombre'].''.$_POST['apellido_paterno'].''.$_POST['apellido_materno'].'.jpg';
		rename($directorio_codigo_barras.'/'.$name_codigo_barras.'.jpg', $directorio_codigo_barras.'/'.$nombre_archivo_codigo_barras);

		$nombre_archivo=$_POST['nombre'].''.$_POST['apellido_paterno'].''.$_POST['apellido_materno'].'.'.$extension;

		$foto = $nombre_archivo;
	}

	

	$observaciones = $_POST['observaciones'];
	$status=$_POST['status'];
	$telefono = $_POST['telefono'];
	$codigo_barras = $_POST['codigo_barras'];
	$domicilio = $_POST['domicilio'];

	$registro = array(
		'id'=>$persona_autorizada_id,'nombre'=>$nombre, 'apellido_paterno'=>$apellido_paterno, 'apellido_materno'=>$apellido_materno,'fecha_nacimiento'=>$fecha_nacimiento,'foto'=>$foto, 'observaciones'=>$observaciones, 'status'=>$status,'telefono'=>$telefono,'codigo_barras'=>$codigo_barras,'domicilio'=>$domicilio);
	$tabla = 'persona_autorizada';
	$persona_autorizada1 = $persona_autorizada_controller->modifica($registro,$tabla);
	if($persona_autorizada1){
		$persona_autorizada_controller->redirect($registro, 'lista', 'persona_autorizada','modifica');
		//header('Location:index.php?seccion=persona_autorizada&accion=modifica&resultado=correcto&operacion=Modifica&persona_autorizada_id='.$persona_autorizada_id);
	}
	else{
		$persona_autorizada_controller->redirect($registro, 'lista', 'persona_autorizada','modifica');
		//header('Location:index.php?seccion=persona_autorizada&accion=modifica&resultado=incorrecto&operacion=Modifica&persona_autorizada_id='.$persona_autorizada_id);	
	}
}
?>
<?php
require_once('controlador_base.php');
require_once('controlador_madre_padre_tutor.php');
require_once('controlador_persona_autorizada.php');

if(file_exists('./config/conexion.php')){
	require_once('./config/conexion.php');
}
if(file_exists('./config/conexion.php')){
	require_once('./modelos.php');
}
class Controlador_Alumno extends Controlador_Base{
	public function lista_alumno(){

		$conexion = new Conexion();
		$conexion->selecciona_base_datos();
		$modelo = new modelos();
		$registro_obtenido = $modelo->genera_lista_alumno();
		$registro_enviar = $registro_obtenido;
		return $registro_enviar;
	}
}

$alumno_controller = new Controlador_Alumno();
$controller_madre_padre_tutor = new Controlador_Madre_Padre_Tutor();
$controller_persona_autorizada = new Controlador_Persona_Autorizada();
$controller_asigna_responsable = new Modelos();

if( $accion == 'asigna_mpt_pa' && $seccion=='alumno'){
	$alumno_id = $_GET['alumno_id'];
	$madre_padre_tutor = $controller_madre_padre_tutor->lista_madre_padre_tutor();
	$persona_autorizada = $controller_persona_autorizada->lista_persona_autorizada();
	$asigna_madre_padre_tutor = $controller_asigna_responsable->genera_lista_asigna_madre_padre_tutor();
	$asigna_persona_autorizada = $controller_asigna_responsable->genera_lista_asigna_persona_autorizada();

	$conexion = new Conexion();
	$conexion->selecciona_base_datos();

	$modelo = new Modelos();

	$alumno = $modelo->obten_por_id('alumno',$alumno_id );
	//$acciones_grupos = $controller_accion_grupo->genera_lista_accion_grupo();
}

if( ($accion == 'lista' || $accion == 'credencial_general') && 
	($seccion == 'alumno' || $seccion == 'credencial')){
	$alumno = $alumno_controller->lista_alumno();
}

if($accion == 'desactiva' && $seccion == 'alumno' ){
	$alumno_id = $_GET['alumno_id'];
	$alumno = $alumno_controller->desactiva($alumno_id,'alumno');
	if($alumno){
		header('Location: index.php?seccion=alumno&accion=lista&resultado=correcto&operacion=Desactiva');
	}
	else{
		header('Location: index.php?seccion=alumno&accion=lista&resultado=incorrecto&operacion=Desactiva');	
	}		
}

if($accion == 'activa' && $seccion == 'alumno'){
	$alumno_id = $_GET['alumno_id'];
	$alumno = $alumno_controller->activa($alumno_id,'alumno');
	if($alumno){
		header('Location: index.php?seccion=alumno&accion=lista&resultado=correcto&operacion=Activa');
	}
	else{
		header('Location: index.php?seccion=alumno&accion=lista&resultado=incorrecto&operacion=Activa');	
	}
}

if(isset($_GET['codigo'])){
	if ($accion == 'modifica' && $seccion == 'alumno' && !empty($_GET['alumno_id'])) {

		$tabla = $_GET['seccion'];
		$id = $_GET['alumno_id'];
		$nombre_codigo_barras = pathinfo($_GET['nombre_codigo'],PATHINFO_FILENAME);

		$directorio_codigo_barras = dirname(__DIR__).'/views/'.$tabla.'/codigo_barras';
		unlink($directorio_codigo_barras.'/'.$nombre_codigo_barras.'.jpg');

		$registro = $alumno_controller->genera_imagen_codigo_barras($tabla,$nombre_codigo_barras);

		$alumno_controller->modifica_codigo_barras($registro,$tabla,$id);

		header('Location:index.php?seccion=alumno&accion=modifica&alumno_id='.$id);
		
	}
}

if($accion == 'modifica' && $seccion == 'alumno'){
	$alumno_id = $_GET['alumno_id'];

	$conexion = new Conexion();
	$conexion->selecciona_base_datos();

	$modelo = new Modelos();

	$alumno = $modelo->obten_por_id('alumno',$alumno_id );

}

if($accion == 'modifica_bd' && $seccion == 'alumno'){
	$alumno_id = $_GET['alumno_id'];
	$nombre = $_POST['nombre'];
	$apellido_paterno = $_POST['apellido_paterno'];
	$apellido_materno = $_POST['apellido_materno'];
	$fecha_nacimiento = $_POST['fecha_nacimiento'];
	
	$directorio_foto = dirname(__DIR__).'/views/alumno/fotos';
	$directorio_codigo_barras = dirname(__DIR__).'/views/alumno/codigo_barras';
	if(empty($_FILES['foto']['tmp_name'])){

		$name_file = $_POST['foto_anterior'];

		$name_codigo_barras = pathinfo($name_file,PATHINFO_FILENAME);
		$extension = pathinfo($name_file, PATHINFO_EXTENSION);

		$nombre_archivo=$_POST['nombre'].''.$_POST['apellido_paterno'].''.$_POST['apellido_materno'].'.'.$extension;
		$nombre_archivo_codigo_barras = $_POST['nombre'].''.$_POST['apellido_paterno'].''.$_POST['apellido_materno'].'.jpg';

		rename($directorio_foto.'/'.$name_file, $directorio_foto.'/'.$nombre_archivo);
		rename($directorio_codigo_barras.'/'.$name_codigo_barras.'.jpg', $directorio_codigo_barras.'/'.$nombre_archivo_codigo_barras);

		$foto = $nombre_archivo;
	}
	else{
		$delete_file = $_POST['foto_anterior'];
		unlink($directorio_foto.'/'.$delete_file);

		$archivo=$_FILES['foto']['tmp_name'];
		$name_file = $_FILES['foto']['name'];
		
		$extension = pathinfo($name_file, PATHINFO_EXTENSION);
		$nombre_archivo=$_POST['nombre'].''.$_POST['apellido_paterno'].''.$_POST['apellido_materno'].'.'.$extension;
		move_uploaded_file($archivo, $directorio_foto."/".$nombre_archivo);


		$name_codigo_barras = pathinfo($_POST['foto_anterior'],PATHINFO_FILENAME);
		$nombre_archivo_codigo_barras = $_POST['nombre'].''.$_POST['apellido_paterno'].''.$_POST['apellido_materno'].'.jpg';
		rename($directorio_codigo_barras.'/'.$name_codigo_barras.'.jpg', $directorio_codigo_barras.'/'.$nombre_archivo_codigo_barras);

		$nombre_archivo=$_POST['nombre'].''.$_POST['apellido_paterno'].''.$_POST['apellido_materno'].'.'.$extension;

		$foto = $nombre_archivo;
	}

	$observaciones = $_POST['observaciones'];
	$status=$_POST['status'];
	$codigo_barras = $_POST['codigo_barras'];
	$tipo_sangre = $_POST['tipo_sangre'];
	$genero = $_POST['genero'];
	$lugar_nacimiento = $_POST['lugar_nacimiento'];
	$fecha_ingreso = $_POST['fecha_ingreso'];

	$registro = array(
		'id'=>$alumno_id,'nombre'=>$nombre, 'apellido_paterno'=>$apellido_paterno, 'apellido_materno'=>$apellido_materno,'fecha_nacimiento'=>$fecha_nacimiento,'foto'=>$foto, 'observaciones'=>$observaciones, 'status'=>$status,'codigo_barras'=>$codigo_barras,'tipo_sangre'=>$tipo_sangre,'genero'=>$genero,'lugar_nacimiento'=>$lugar_nacimiento,'fecha_ingreso'=>$fecha_ingreso);
	$tabla = 'alumno';
	$alumno1 = $alumno_controller->modifica($registro,$tabla);
	if($alumno1){
		$alumno_controller->redirect($registro, 'lista', 'alumno','modifica');
		//header('Location:index.php?seccion=alumno&accion=modifica&resultado=correcto&operacion=Modifica&alumno_id='.$alumno_id);
	}
	else{
		$alumno_controller->redirect($registro, 'lista', 'alumno','modifica');
		//header('Location:index.php?seccion=alumno&accion=modifica&resultado=incorrecto&operacion=Modifica&alumno_id='.$alumno_id);	
	}
}
?>
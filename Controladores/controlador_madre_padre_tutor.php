<?php
require_once('controlador_base.php');

if(file_exists('./config/conexion.php')){
	require_once('./config/conexion.php');
}
if(file_exists('./config/conexion.php')){
	require_once('./modelos.php');
}
class Controlador_Madre_Padre_Tutor extends Controlador_Base{
	public function lista_madre_padre_tutor(){

		$conexion = new Conexion();
		$conexion->selecciona_base_datos();
		$modelo = new modelos();
		$registro_obtenido = $modelo->genera_lista_madre_padre_tutor();
		$registro_enviar = $registro_obtenido;
		return $registro_enviar;
	}
}

$madre_padre_tutor_controller = new Controlador_Madre_Padre_Tutor();

if( ($accion == 'lista' || $accion == 'credencial_general') && 
	($seccion == 'madre_padre_tutor' || $seccion == 'credencial')){
	$madre_padre_tutor = $madre_padre_tutor_controller->lista_madre_padre_tutor();
}

if($accion == 'desactiva' && $seccion == 'madre_padre_tutor' ){
	$madre_padre_tutor_id = $_GET['madre_padre_tutor_id'];
	$madre_padre_tutor = $madre_padre_tutor_controller->desactiva($madre_padre_tutor_id,'madre_padre_tutor');
	if($madre_padre_tutor){
		header('Location: index.php?seccion=madre_padre_tutor&accion=lista&resultado=correcto&operacion=Desactiva');
	}
	else{
		header('Location: index.php?seccion=madre_padre_tutor&accion=lista&resultado=incorrecto&operacion=Desactiva');	
	}		
}

if($accion == 'activa' && $seccion == 'madre_padre_tutor'){
	$madre_padre_tutor_id = $_GET['madre_padre_tutor_id'];
	$madre_padre_tutor = $madre_padre_tutor_controller->activa($madre_padre_tutor_id,'madre_padre_tutor');
	if($madre_padre_tutor){
		header('Location: index.php?seccion=madre_padre_tutor&accion=lista&resultado=correcto&operacion=Activa');
	}
	else{
		header('Location: index.php?seccion=madre_padre_tutor&accion=lista&resultado=incorrecto&operacion=Activa');	
	}
}

if(isset($_GET['codigo'])){
	if ($accion == 'modifica' && $seccion == 'madre_padre_tutor' && !empty($_GET['madre_padre_tutor_id'])) {

		$tabla = $_GET['seccion'];
		$id = $_GET['madre_padre_tutor_id'];
		$nombre_codigo_barras = pathinfo($_GET['nombre_codigo'],PATHINFO_FILENAME);

		$directorio_codigo_barras = dirname(__DIR__).'/views/'.$tabla.'/codigo_barras';
		unlink($directorio_codigo_barras.'/'.$nombre_codigo_barras.'.jpg');

		$registro = $madre_padre_tutor_controller->genera_imagen_codigo_barras($tabla,$nombre_codigo_barras);

		$madre_padre_tutor_controller->modifica_codigo_barras($registro,$tabla,$id);

		header('Location:index.php?seccion=madre_padre_tutor&accion=modifica&madre_padre_tutor_id='.$id);
		
	}
}

if($accion == 'modifica' && $seccion == 'madre_padre_tutor'){
	$madre_padre_tutor_id = $_GET['madre_padre_tutor_id'];

	$conexion = new Conexion();
	$conexion->selecciona_base_datos();

	$modelo = new Modelos();

	$madre_padre_tutor = $modelo->obten_por_id('madre_padre_tutor',$madre_padre_tutor_id );

}

if($accion == 'modifica_bd' && $seccion == 'madre_padre_tutor'){
	$madre_padre_tutor_id = $_GET['madre_padre_tutor_id'];
	$nombre = $_POST['nombre'];
	$apellido_paterno = $_POST['apellido_paterno'];
	$apellido_materno = $_POST['apellido_materno'];
	$fecha_nacimiento = $_POST['fecha_nacimiento'];
	
	$directorio_foto = dirname(__DIR__).'/views/madre_padre_tutor/fotos';
	$directorio_codigo_barras = dirname(__DIR__).'/views/madre_padre_tutor/codigo_barras';
	if(empty($_FILES['foto']['tmp_name'])){

		$name_file = $_POST['foto_anterior'];

		$name_codigo_barras = pathinfo($name_file,PATHINFO_FILENAME);
		$extension = pathinfo($name_file, PATHINFO_EXTENSION);

		$nombre_archivo=$_POST['nombre'].''.$_POST['apellido_paterno'].''.$_POST['apellido_materno'].'.'.$extension;
		$nombre_archivo_codigo_barras = $_POST['nombre'].''.$_POST['apellido_paterno'].''.$_POST['apellido_materno'].'.jpg';

		rename($directorio_foto.'/'.$name_file, $directorio_foto.'/'.$nombre_archivo);
		rename($directorio_codigo_barras.'/'.$name_codigo_barras.'.jpg', $directorio_codigo_barras.'/'.$nombre_archivo_codigo_barras);

		$foto = $nombre_archivo;
	}
	else{
		$delete_file = $_POST['foto_anterior'];
		unlink($directorio_foto.'/'.$delete_file);

		$archivo=$_FILES['foto']['tmp_name'];
		$name_file = $_FILES['foto']['name'];
		
		$extension = pathinfo($name_file, PATHINFO_EXTENSION);
		$nombre_archivo=$_POST['nombre'].''.$_POST['apellido_paterno'].''.$_POST['apellido_materno'].'.'.$extension;
		move_uploaded_file($archivo, $directorio_foto."/".$nombre_archivo);


		$name_codigo_barras = pathinfo($_POST['foto_anterior'],PATHINFO_FILENAME);
		$nombre_archivo_codigo_barras = $_POST['nombre'].''.$_POST['apellido_paterno'].''.$_POST['apellido_materno'].'.jpg';
		rename($directorio_codigo_barras.'/'.$name_codigo_barras.'.jpg', $directorio_codigo_barras.'/'.$nombre_archivo_codigo_barras);

		$nombre_archivo=$_POST['nombre'].''.$_POST['apellido_paterno'].''.$_POST['apellido_materno'].'.'.$extension;

		$foto = $nombre_archivo;
	}

	$observaciones = $_POST['observaciones'];
	$status=$_POST['status'];
	$telefono = $_POST['telefono'];
	$codigo_barras = $_POST['codigo_barras'];
	$domicilio = $_POST['domicilio'];

	$registro = array(
		'id'=>$madre_padre_tutor_id,'nombre'=>$nombre, 'apellido_paterno'=>$apellido_paterno, 'apellido_materno'=>$apellido_materno,'fecha_nacimiento'=>$fecha_nacimiento,'foto'=>$foto, 'observaciones'=>$observaciones, 'status'=>$status,'telefono'=>$telefono,'codigo_barras'=>$codigo_barras,'domicilio'=>$domicilio);
	$tabla = 'madre_padre_tutor';
	$madre_padre_tutor1 = $madre_padre_tutor_controller->modifica($registro,$tabla);
	if($madre_padre_tutor1){
		$madre_padre_tutor_controller->redirect($registro, 'lista', 'madre_padre_tutor','modifica');
		//header('Location:index.php?seccion=madre_padre_tutor&accion=modifica&resultado=correcto&operacion=Modifica&madre_padre_tutor_id='.$madre_padre_tutor_id);
	}
	else{
		$madre_padre_tutor_controller->redirect($registro, 'lista', 'madre_padre_tutor','modifica');
		//header('Location:index.php?seccion=madre_padre_tutor&accion=modifica&resultado=incorrecto&operacion=Modifica&madre_padre_tutor_id='.$madre_padre_tutor_id);	
	}
}
?>
<?php
require_once('./Controladores/controlador_madre_padre_tutor.php');
require_once('./Controladores/controlador_persona_autorizada.php');
require_once('./Controladores/controlador_alumno.php');

if(file_exists('./config/conexion.php')){
	require_once('./config/conexion.php');
}
if(file_exists('./modelos.php')){
	require_once('./modelos.php');
}


class Controlador_Registro{	

	public function alta_bd(){
		$controller_modelos = new Modelos();

		$conexion = new Conexion();
		$conexion->selecciona_base_datos();
		$link = $conexion->link;

		$codigo_barras = $_POST['codigo_barras'];
		//$codigo_barras = 'MPT_595d54624bf0c';
		$operacion_tabla = explode("_", $codigo_barras);

		$accion = $_POST['accion'];
		//$accion = 'Responsable';
		$archivo_ninos = "contador_ninos.txt";
		$fp_ninos = fopen($archivo_ninos,"r");
		$contador_ninos = fgets($fp_ninos, 26);
		fclose($fp_ninos);
		$archivo_ninas = "contador_ninas.txt";
		$fp_ninas = fopen($archivo_ninas,"r");
		$contador_ninas = fgets($fp_ninas, 26);
		fclose($fp_ninas);
		$archivo_personal = "contador_personal.txt";
		$fp_personal = fopen($archivo_personal,"r");
		$contador_personal = fgets($fp_personal, 26);
		fclose($fp_personal);

		if($accion == 'Entrada'){
			if($operacion_tabla[0] == 'A'){
			
				$busca_alumno = $controller_modelos->obten_por_codigo_barras('alumno',$codigo_barras);

				if (!empty($busca_alumno)) {

					if($busca_alumno[0]['status'] == 0){
						header('Content-type: application/json; charset=utf-8');
						echo json_encode(array("existe"=>false));
						exit();
					}

					$genera_fecha = mysqli_query($link,"SELECT CURDATE()");
					$genera_hora = mysqli_query($link,"SELECT NOW()");
					$fecha = '';
					$hora = '';

					$alumno_id = $busca_alumno[0]['id'];

					while($row=mysqli_fetch_array($genera_fecha))
					{ 
						$fecha = $row['CURDATE()'] ;
					}
					while($row=mysqli_fetch_array($genera_hora))
					{ 
						$hora = $row['NOW()'] ;
					}

					$foto_alumno = './views/alumno/fotos/'.$busca_alumno[0]['foto'];

					$consulta_insercion = "INSERT INTO registro(codigo_barras,alumno_id,fecha,hora) VALUES('$codigo_barras','$alumno_id','$fecha','$hora')";
					//$link->query($consulta_insercion);

					$fotos_madre_padre_tutor = $controller_modelos->obten_fotos('madre_padre_tutor',$alumno_id);
					$fotos_persona_autorizada = $controller_modelos->obten_fotos('persona_autorizada',$alumno_id);

					$fotos = array_merge_recursive($fotos_madre_padre_tutor, $fotos_persona_autorizada);

					$registro_entrada_salida = $controller_modelos->obten_por_codigo_barras_fecha($codigo_barras);
					$total_registro = count($registro_entrada_salida);

					if ( $total_registro%2 == 0 ){
						//si es par registrara nueva entrada
					    //echo "el $numero es par";
					    //$link->query($consulta_insercion);

						if($link->error){echo $link->error;}
						else{
							if(!empty($fotos)){
							//$link->query($consulta_insercion);
								$registro_id = $link->insert_id; //echo $registro_id;
								header('Content-type: application/json; charset=utf-8');
								echo json_encode(
									array(
										"existe"=>true,
										"id"=>$alumno_id,
										"codigo_barras"=>$codigo_barras,
										"foto_registro"=>$foto_alumno,
										"fotos"=>
											array(
												'0' => array('foto' => './views/img/correcto.png','nombre' => '','apellido_paterno' => '','apellido_materno' => '' )),
										"consulta"=>$consulta_insercion,
										"genero"=>$busca_alumno[0]['genero'],
										"operacion_contador"=>"suma"
											));
								exit();
							}
							else{
								header('Content-type: application/json; charset=utf-8');
								echo json_encode(array("existe"=>true,"id"=>$alumno_id,"foto_registro"=>$foto_alumno,"fotos"=>array('0' => array('foto' => './views/img/error.jpeg','nombre' => '','apellido_paterno' => '','apellido_materno' => '' ) ),
									"accion_realizada"=>"Recargar"));
								exit();
							}
						}
					}
					else{
						$filtro = array("-", " ", ":");

						$hora_registro_anterior = str_replace($filtro,'', end( end($registro_entrada_salida) ) );
						$hora_registro_actual = str_replace($filtro,'', $hora );
						$hora_registro_act = substr(substr($hora_registro_actual, 8),0,-4);
						$hora_registro__ant = substr(substr($hora_registro_anterior, 8),0,-4);

						$minutos_registro_act = ($hora_registro_act*60)+substr(substr($hora_registro_actual, 8),2,-2);
						$minutos_registro_ant = ($hora_registro__ant*60)+substr(substr($hora_registro_anterior, 8),2,-2);


						$diferencia_hora = abs( $hora_registro_act - $hora_registro__ant );
						$diferencia_minutos = abs( $minutos_registro_act - $minutos_registro_ant );

						if ($diferencia_minutos <= 59) {
							$diferencia_minutos = $diferencia_minutos;
						}else{
							$diferencia_minutos = $diferencia_minutos%60;
						}

						if ($diferencia_hora > 0 || $diferencia_minutos >= 2 ) {
						
							//$link->query($consulta_insercion);
							if($link->error){echo $link->error;}
							else{
								if(!empty($fotos)){
									$registro_id = $link->insert_id; //echo $registro_id;
									header('Content-type: application/json; charset=utf-8');
									echo json_encode(array(
										"existe"=>true,
										"id"=>$alumno_id,
										"codigo_barras"=>$codigo_barras,
										"foto_registro"=>$foto_alumno,
										"fotos"=>
											array(
												'0' => array('foto' => './views/img/correcto.png','nombre' => '','apellido_paterno' => '','apellido_materno' => '' )),
										"consulta"=>$consulta_insercion,
										"genero"=>$busca_alumno[0]['genero'],
										"operacion_contador"=>"resta"));
									exit();
								}
								else{
									header('Content-type: application/json; charset=utf-8');
									echo json_encode(array("existe"=>true,"id"=>$alumno_id,"foto_registro"=>$foto_alumno,"fotos"=>array('0' => array('foto' => './views/img/error.jpeg','nombre' => '','apellido_paterno' => '','apellido_materno' => '' ) ),
									"accion_realizada"=>"Recargar"));
									exit();
								}
							}

						}
						else{
							header('Content-type: application/json; charset=utf-8');
							echo json_encode(array("existe"=>false));
							exit();
						}
					}
				}
				else{
					header('Content-type: application/json; charset=utf-8');
					echo json_encode(array("existe"=>false));
					exit();
				}
			}elseif($operacion_tabla[0] == 'P'){
				
				$busca_personal = $controller_modelos->obten_por_codigo_barras('personal',$codigo_barras);

				if (!empty($busca_personal)) {
					
					if($busca_personal[0]['status'] == '0'){
						header('Content-type: application/json; charset=utf-8');
						echo json_encode(array("existe"=>false));
						exit();
					}

					$genera_fecha = mysqli_query($link,"SELECT CURDATE()");
					$genera_hora = mysqli_query($link,"SELECT NOW()");
					$fecha = '';
					$hora = '';

					$personal_id = $busca_personal[0]['id'];

					while($row=mysqli_fetch_array($genera_fecha))
					{ 
						$fecha = $row['CURDATE()'] ;
					}
					while($row=mysqli_fetch_array($genera_hora))
					{ 
						$hora = $row['NOW()'] ;
					}

					$foto_personal = './views/personal/fotos/'.$busca_personal[0]['foto'];
					$consulta_insercion = "INSERT INTO registro(codigo_barras,personal_id,fecha,hora) VALUES('$codigo_barras','$personal_id','$fecha','$hora')";

					$registro_entrada_salida = $controller_modelos->obten_por_codigo_barras_fecha($codigo_barras,'IS NULL');
					$total_registro = count($registro_entrada_salida);

					if ( $total_registro%2 == 0 ){
						//si es par registrara nueva entrada
					    //echo "el $numero es par";
					    $link->query($consulta_insercion);

						if($link->error){echo $link->error;}
						else{
							++$contador_personal;

							$fp_personal = fopen($archivo_personal,"w+");
							fwrite($fp_personal, $contador_personal, 26);
							fclose($fp_personal);
			                
							header('Content-type: application/json; charset=utf-8');
							echo json_encode(
								array(
									"existe"=>true,
									"id"=>$personal_id,
									"foto_registro"=>$foto_personal,
									"fotos"=>
										array(
											'0' => array('foto' => './views/img/correcto.png','nombre' => '','apellido_paterno' => '','apellido_materno' => '' )),
									"contador"=>$contador_personal,
									"accion_realizada"=>"Recargar"
										));
							exit();
						}
					}
					else{
						//print_r( end( end($registro_entrada_salida) ) );
						$filtro = array("-", " ", ":");

						$hora_registro_anterior = str_replace($filtro,'', end( end($registro_entrada_salida) ) );
						$hora_registro_actual = str_replace($filtro,'', $hora );
						//SELECT ((now() - 20170707095402) - 4000) DIV 100 AS minutos;
						$hora_registro_act = substr(substr($hora_registro_actual, 8),0,-4);
						$hora_registro__ant = substr(substr($hora_registro_anterior, 8),0,-4);

						$minutos_registro_act = ($hora_registro_act*60)+substr(substr($hora_registro_actual, 8),2,-2);
						$minutos_registro_ant = ($hora_registro__ant*60)+substr(substr($hora_registro_anterior, 8),2,-2);


						$diferencia_hora = abs( $hora_registro_act - $hora_registro__ant );
						$diferencia_minutos = abs( $minutos_registro_act - $minutos_registro_ant );

						if ($diferencia_minutos <= 59) {
							$diferencia_minutos = $diferencia_minutos;
						}else{
							$diferencia_minutos = $diferencia_minutos%60;
						}
						//echo $diferencia_hora.'<br>';
						//echo $diferencia_minutos;

						if ($diferencia_hora > 0 || $diferencia_minutos >= 2 ) {
						
							$link->query($consulta_insercion);
							if($link->error){echo $link->error;}
							else{
								--$contador_personal;

								$fp_personal = fopen($archivo_personal,"w+");
								fwrite($fp_personal, $contador_personal, 26);
								fclose($fp_personal);
				                
								header('Content-type: application/json; charset=utf-8');
								echo json_encode(
									array(
										"existe"=>true,
										"id"=>$personal_id,
										"foto_registro"=>$foto_personal,
										"fotos"=>
											array(
												'0' => array('foto' => './views/img/correcto.png','nombre' => '','apellido_paterno' => '','apellido_materno' => '' )),
										"contador"=>$contador_personal,
										"accion_realizada"=>"Recargar"
											));
								exit();
							}

						}
						else{
							header('Content-type: application/json; charset=utf-8');
							echo json_encode(array("existe"=>false));
							exit();
						}

						/*
						$link->query($consulta_insercion);
						if($link->error){echo $link->error;}
						else{
							--$contador;

							$fp = fopen($archivo,"w+");
							fwrite($fp, $contador, 26);
							fclose($fp);
			                
							header('Content-type: application/json; charset=utf-8');
							echo json_encode(
								array(
									"existe"=>true,
									"id"=>$personal_id,
									"foto_registro"=>$foto_personal,
									"fotos"=>
										array(
											'0' => array('foto' => './views/img/correcto.png','nombre' => '','apellido_paterno' => '','apellido_materno' => '' )),
									"contador"=>$contador
										));
							exit();
						}*/
					}

					
				}
				else{
					header('Content-type: application/json; charset=utf-8');
					echo json_encode(array("existe"=>false));
					exit();
				}
			}else{
				header('Content-type: application/json; charset=utf-8');
				echo json_encode(array("existe"=>false));
				exit();
			}
		}elseif($accion == 'Responsable'){
			$alumno_id = $_POST['alumno_id'];
			$codigo_barras_alumno = $_POST['codigo_barras_alumno'];
			$consulta_alumno = $_POST['consulta'];
			$operacion_contador = $_POST['operacion_contador'];
			$genero = $_POST['genero'];
			if($operacion_tabla[0] == 'MPT'){
				
				$busca_madre_padre_tutor = $controller_modelos->obten_por_codigo_barras('madre_padre_tutor',$codigo_barras);

				if (!empty($busca_madre_padre_tutor)) {
					if($busca_madre_padre_tutor[0]['status'] == 0){
						header('Content-type: application/json; charset=utf-8');
						echo json_encode(array("existe"=>false));
						exit();
					}

					$madre_padre_tutor_id = $busca_madre_padre_tutor[0]['id'];

					$comprueba_responsable = $controller_modelos->comprueba_responsable('madre_padre_tutor',$alumno_id, $codigo_barras_alumno,$madre_padre_tutor_id,$codigo_barras);

					if(!empty($comprueba_responsable)){
						$genera_fecha = mysqli_query($link,"SELECT CURDATE()");
						$genera_hora = mysqli_query($link,"SELECT NOW()");
						$fecha = '';
						$hora = '';

						while($row=mysqli_fetch_array($genera_fecha))
						{ 
							$fecha = $row['CURDATE()'] ;
						}
						while($row=mysqli_fetch_array($genera_hora))
						{ 
							$hora = $row['NOW()'] ;
						}

						$foto_madre_padre_tutor = './views/madre_padre_tutor/fotos/'.$busca_madre_padre_tutor[0]['foto'];
						$consulta_insercion = "INSERT INTO registro(codigo_barras,alumno_id,madre_padre_tutor_id,fecha,hora) VALUES('$codigo_barras','$alumno_id','$madre_padre_tutor_id','$fecha','$hora')";

						
						$link->query($consulta_alumno);
						$link->query($consulta_insercion);

						if($link->error){echo $link->error;}
						else{
							if ($operacion_contador == 'suma') {
								if($genero == 'hombre'){
									++$contador_ninos;

									$fp_ninos = fopen($archivo_ninos,"w+");
									fwrite($fp_ninos, $contador_ninos, 26);
									fclose($fp_ninos);
								}elseif ($genero == 'mujer') {
									++$contador_ninas;

									$fp_ninas = fopen($archivo_ninas,"w+");
									fwrite($fp_ninas, $contador_ninas, 26);
									fclose($fp_ninas);
								}
								
							}elseif ($operacion_contador == 'resta') {
								if($genero == 'hombre'){
									--$contador_ninos;

									$fp_ninos = fopen($archivo_ninos,"w+");
									fwrite($fp_ninos, $contador_ninos, 26);
									fclose($fp_ninos);
								}elseif ($genero == 'mujer') {
									--$contador_ninas;

									$fp_ninas = fopen($archivo_ninas,"w+");
									fwrite($fp_ninas, $contador_ninas, 26);
									fclose($fp_ninas);
								}
								
							}
							
				                
							header('Content-type: application/json; charset=utf-8');
							echo json_encode(
								array(
									"existe"=>true,
									"id"=>$madre_padre_tutor_id,
									"foto_registro"=>$foto_madre_padre_tutor,
									"fotos"=>
										array(
											'0' => array('foto' => $comprueba_responsable[0]['foto'],'nombre' => $comprueba_responsable[0]['nombre'],'apellido_paterno' => $comprueba_responsable[0]['apellido_paterno'],'apellido_materno' => $comprueba_responsable[0]['apellido_materno'] )),
									"accion_realizada"=>"Recargar"
										));
							exit();
							
						}

					}
					else{
						header('Content-type: application/json; charset=utf-8');
						echo json_encode(array("existe"=>false));
						exit();
					}
				}
				else{
					header('Content-type: application/json; charset=utf-8');
					echo json_encode(array("existe"=>false));
					exit();
				}
			}elseif($operacion_tabla[0] == 'PA'){
				
				$busca_persona_autorizada = $controller_modelos->obten_por_codigo_barras('persona_autorizada',$codigo_barras);

				if (!empty($busca_persona_autorizada)) {
					if($busca_persona_autorizada[0]['status'] == 0){
						header('Content-type: application/json; charset=utf-8');
						echo json_encode(array("existe"=>false));
						exit();
					}
					
					$persona_autorizada_id = $busca_persona_autorizada[0]['id'];

					$comprueba_responsable = $controller_modelos->comprueba_responsable('persona_autorizada',$alumno_id, $codigo_barras_alumno,$persona_autorizada_id,$codigo_barras);

					if(!empty($comprueba_responsable)){
						$genera_fecha = mysqli_query($link,"SELECT CURDATE()");
						$genera_hora = mysqli_query($link,"SELECT NOW()");
						$fecha = '';
						$hora = '';

						while($row=mysqli_fetch_array($genera_fecha))
						{ 
							$fecha = $row['CURDATE()'] ;
						}
						while($row=mysqli_fetch_array($genera_hora))
						{ 
							$hora = $row['NOW()'] ;
						}

						$foto_persona_autorizada = './views/persona_autorizada/fotos/'.$busca_persona_autorizada[0]['foto'];
						$consulta_insercion = "INSERT INTO registro(codigo_barras,alumno_id,persona_autorizada_id,fecha,hora) VALUES('$codigo_barras','$alumno_id','$persona_autorizada_id','$fecha','$hora')";

						
						$link->query($consulta_alumno);
						$link->query($consulta_insercion);

						if($link->error){echo $link->error;}
						else{
							if ($operacion_contador == 'suma') {
								if($genero == 'hombre'){
									++$contador_ninos;

									$fp_ninos = fopen($archivo_ninos,"w+");
									fwrite($fp_ninos, $contador_ninos, 26);
									fclose($fp_ninos);
								}elseif ($genero == 'mujer') {
									++$contador_ninas;

									$fp_ninas = fopen($archivo_ninas,"w+");
									fwrite($fp_ninas, $contador_ninas, 26);
									fclose($fp_ninas);
								}
								
							}elseif ($operacion_contador == 'resta') {
								if($genero == 'hombre'){
									--$contador_ninos;

									$fp_ninos = fopen($archivo_ninos,"w+");
									fwrite($fp_ninos, $contador_ninos, 26);
									fclose($fp_ninos);
								}elseif ($genero == 'mujer') {
									--$contador_ninas;

									$fp_ninas = fopen($archivo_ninas,"w+");
									fwrite($fp_ninas, $contador_ninas, 26);
									fclose($fp_ninas);
								}
								
							}
				                
							header('Content-type: application/json; charset=utf-8');
							echo json_encode(
								array(
									"existe"=>true,
									"id"=>$persona_autorizada_id,
									"foto_registro"=>$foto_persona_autorizada,
									"fotos"=>
										array(
											'0' => array('foto' => $comprueba_responsable[0]['foto'],'nombre' => $comprueba_responsable[0]['nombre'],'apellido_paterno' => $comprueba_responsable[0]['apellido_paterno'],'apellido_materno' => $comprueba_responsable[0]['apellido_materno'] )),
									"accion_realizada"=>"Recargar"
										));
							exit();
							
						}

					}
					else{
						header('Content-type: application/json; charset=utf-8');
						echo json_encode(array("existe"=>false));
						exit();
					}
				}
				else{
					header('Content-type: application/json; charset=utf-8');
					echo json_encode(array("existe"=>false));
					exit();
				}
			}elseif($operacion_tabla[0] == 'P'){
				$busca_alumno = $controller_modelos->obten_por_codigo_barras('alumno',$codigo_barras_alumno);
				$busca_personal = $controller_modelos->obten_por_codigo_barras('personal',$codigo_barras);

				if (!empty($busca_personal)) {
					if($busca_personal[0]['status'] == 0){
						header('Content-type: application/json; charset=utf-8');
						echo json_encode(array("existe"=>false));
						exit();
					}
					
					$personal_id = $busca_personal[0]['id'];

					$genera_fecha = mysqli_query($link,"SELECT CURDATE()");
					$genera_hora = mysqli_query($link,"SELECT NOW()");
					$fecha = '';
					$hora = '';

					while($row=mysqli_fetch_array($genera_fecha)){ 
						$fecha = $row['CURDATE()'] ;
					}
					while($row=mysqli_fetch_array($genera_hora)){ 
						$hora = $row['NOW()'] ;
					}

					$foto_personal = './views/personal/fotos/'.$busca_personal[0]['foto'];
					$consulta_insercion = "INSERT INTO registro(codigo_barras,alumno_id,personal_id,fecha,hora) VALUES('$codigo_barras','$alumno_id','$personal_id','$fecha','$hora')";

						
					$link->query($consulta_alumno);
					$link->query($consulta_insercion);

					if($link->error){echo $link->error;}
					else{
							if ($operacion_contador == 'suma') {
								if($genero == 'hombre'){
									++$contador_ninos;

									$fp_ninos = fopen($archivo_ninos,"w+");
									fwrite($fp_ninos, $contador_ninos, 26);
									fclose($fp_ninos);
								}elseif ($genero == 'mujer') {
									++$contador_ninas;

									$fp_ninas = fopen($archivo_ninas,"w+");
									fwrite($fp_ninas, $contador_ninas, 26);
									fclose($fp_ninas);
								}
								
							}elseif ($operacion_contador == 'resta') {
								if($genero == 'hombre'){
									--$contador_ninos;

									$fp_ninos = fopen($archivo_ninos,"w+");
									fwrite($fp_ninos, $contador_ninos, 26);
									fclose($fp_ninos);
								}elseif ($genero == 'mujer') {
									--$contador_ninas;

									$fp_ninas = fopen($archivo_ninas,"w+");
									fwrite($fp_ninas, $contador_ninas, 26);
									fclose($fp_ninas);
								}
								
							}
				                
							header('Content-type: application/json; charset=utf-8');
							echo json_encode(
								array(
									"existe"=>true,
									"id"=>$personal_id,
									"foto_registro"=>$foto_personal,
									"fotos"=>
										array(
											'0' => array('foto' => './views/alumno/fotos/'.$busca_alumno[0]['foto'],'nombre' => $busca_alumno[0]['nombre'],'apellido_paterno' => $busca_alumno[0]['apellido_paterno'],'apellido_materno' => $busca_alumno[0]['apellido_materno'] )),
									"accion_realizada"=>"Recargar"
										));
							exit();
						
					}

				}
				else{
					header('Content-type: application/json; charset=utf-8');
					echo json_encode(array("existe"=>false));
					exit();
				}
			}else{
				header('Content-type: application/json; charset=utf-8');
				echo json_encode(array("existe"=>false));
				exit();
			}
		}else{
			header('Content-type: application/json; charset=utf-8');
			echo json_encode(array("existe"=>false));
			exit();
		}

		/*

			if($operacion_tabla[0] == 'A'){
				
				$busca_alumno = $controller_modelos->obten_por_codigo_barras('alumno',$codigo_barras);

				if (!empty($busca_alumno)) {

					$genera_fecha = mysqli_query($link,"SELECT CURDATE()");
					$genera_hora = mysqli_query($link,"SELECT NOW()");
					$fecha = '';
					$hora = '';

					$alumno_id = $busca_alumno[0]['id'];

					while($row=mysqli_fetch_array($genera_fecha))
					{ 
						$fecha = $row['CURDATE()'] ;
					}
					while($row=mysqli_fetch_array($genera_hora))
					{ 
						$hora = $row['NOW()'] ;
					}

					$foto_alumno = './views/alumno/fotos/'.$busca_alumno[0]['foto'];

					$consulta_insercion = "INSERT INTO registro(codigo_barras,alumno_id,fecha,hora) VALUES('$codigo_barras','$alumno_id','$fecha','$hora')";
					//$link->query($consulta_insercion);

					$fotos_madre_padre_tutor = $controller_modelos->obten_fotos('madre_padre_tutor',$alumno_id);
					$fotos_persona_autorizada = $controller_modelos->obten_fotos('persona_autorizada',$alumno_id);

					$fotos = array_merge_recursive($fotos_madre_padre_tutor, $fotos_persona_autorizada);

					if($link->error){}
					else{
						if(!empty($fotos)){
							//$link->query($consulta_insercion);
							$registro_id = $link->insert_id; //echo $registro_id;
							header('Content-type: application/json; charset=utf-8');
							echo json_encode(array("resultado"=>true,"alumno_id"=>$alumno_id,"foto_registro"=>$foto_alumno,"fotos"=>$fotos,"consulta"=>$consulta_insercion));
							exit();
						}
						else{
							header('Content-type: application/json; charset=utf-8');
							echo json_encode(array("resultado"=>true,"alumno_id"=>$alumno_id,"foto_registro"=>$foto_alumno,"fotos"=>array('0' => array('foto' => './views/img/error.jpeg','nombre' => '','apellido_paterno' => '','apellido_materno' => '' ) )));
							exit();
						}
					}
					
				}
				else{
					header('Content-type: application/json; charset=utf-8');
					echo json_encode(array("resultado"=>false));
					exit();
				}
			}elseif($operacion_tabla[0] == 'MPT'){
				$alumno_id = $_POST['alumno_id'];
				//$alumno_id = 2;
				
				$busca_madre_padre_tutor = $controller_modelos->obten_por_codigo_barras('madre_padre_tutor',$codigo_barras);

				if (!empty($busca_madre_padre_tutor)) {

					$genera_fecha = mysqli_query($link,"SELECT CURDATE()");
					$genera_hora = mysqli_query($link,"SELECT NOW()");
					$fecha = '';
					$hora = '';

					$madre_padre_tutor_id = $busca_madre_padre_tutor[0]['id'];

					while($row=mysqli_fetch_array($genera_fecha))
					{ 
						$fecha = $row['CURDATE()'] ;
					}
					while($row=mysqli_fetch_array($genera_hora))
					{ 
						$hora = $row['NOW()'] ;
					}

					$foto_madre_padre_tutor = './views/madre_padre_tutor/fotos/'.$busca_madre_padre_tutor[0]['foto'];
					//enviar post alumno_id
					$consulta_insercion = "INSERT INTO registro(codigo_barras,alumno_id,madre_padre_tutor_id,fecha,hora) VALUES('$codigo_barras',$alumno_id,'$madre_padre_tutor_id','$fecha','$hora')";
					//$link->query($consulta_insercion);

					$fotos_alumno = $controller_modelos->obten_fotos_alumno('madre_padre_tutor',$madre_padre_tutor_id);

					if($link->error){echo $link->error;}
					else{
						if(!empty($fotos_alumno)){
							$link->query($consulta_insercion);
							$registro_id = $link->insert_id; //echo $registro_id;
							header('Content-type: application/json; charset=utf-8');
							echo json_encode(array("resultado"=>true,"alumno_id"=>$madre_padre_tutor_id,"foto_registro"=>$foto_madre_padre_tutor,"fotos"=>$fotos_alumno));
							exit();
						}
						else{
							header('Content-type: application/json; charset=utf-8');
							echo json_encode(array("resultado"=>true,"alumno_id"=>$madre_padre_tutor_id,"foto_registro"=>$foto_madre_padre_tutor,"fotos"=>array('0' => array('foto' => './views/img/error.jpeg','nombre' => '','apellido_paterno' => '','apellido_materno' => '' ) )));
							exit();
						}
					}
					
				}
				else{
					header('Content-type: application/json; charset=utf-8');
					echo json_encode(array("resultado"=>false));
					exit();
				}
			}elseif($operacion_tabla[0] == 'PA'){
				$alumno_id = $_POST['alumno_id'];;
				
				$busca_persona_autorizada = $controller_modelos->obten_por_codigo_barras('persona_autorizada',$codigo_barras);

				if (!empty($busca_persona_autorizada)) {

					$genera_fecha = mysqli_query($link,"SELECT CURDATE()");
					$genera_hora = mysqli_query($link,"SELECT NOW()");
					$fecha = '';
					$hora = '';

					$persona_autorizada_id = $busca_persona_autorizada[0]['id'];

					while($row=mysqli_fetch_array($genera_fecha))
					{ 
						$fecha = $row['CURDATE()'] ;
					}
					while($row=mysqli_fetch_array($genera_hora))
					{ 
						$hora = $row['NOW()'] ;
					}

					$foto_persona_autorizada = './views/persona_autorizada/fotos/'.$busca_persona_autorizada[0]['foto'];
					//enviar post alumno_id
					$consulta_insercion = "INSERT INTO registro(codigo_barras,alumno_id,persona_autorizada_id,fecha,hora) VALUES('$codigo_barras',$alumno_id,'$persona_autorizada_id','$fecha','$hora')";
					//$link->query($consulta_insercion);

					$fotos_alumno = $controller_modelos->obten_fotos_alumno('persona_autorizada',$persona_autorizada_id);

					if($link->error){echo $link->error;}
					else{
						if(!empty($fotos_alumno)){
							$link->query($consulta_insercion);
							$registro_id = $link->insert_id; //echo $registro_id;
							header('Content-type: application/json; charset=utf-8');
							echo json_encode(array("resultado"=>true,"alumno_id"=>$persona_autorizada_id,"foto_registro"=>$foto_persona_autorizada,"fotos"=>$fotos_alumno));
							exit();
						}
						else{
							header('Content-type: application/json; charset=utf-8');
							echo json_encode(array("resultado"=>true,"alumno_id"=>$persona_autorizada_id,"foto_registro"=>$foto_persona_autorizada,"fotos"=>array('0' => array('foto' => './views/img/error.jpeg','nombre' => '','apellido_paterno' => '','apellido_materno' => '' ) )));
							exit();
						}
					}
					
				}
				else{
					header('Content-type: application/json; charset=utf-8');
					echo json_encode(array("resultado"=>false));
					exit();
				}
			}elseif($operacion_tabla[0] == 'P'){
				$alumno_id = $_POST['alumno_id'];;
				
				$busca_personal = $controller_modelos->obten_por_codigo_barras('personal',$codigo_barras);

				if (!empty($busca_personal)) {

					$genera_fecha = mysqli_query($link,"SELECT CURDATE()");
					$genera_hora = mysqli_query($link,"SELECT NOW()");
					$fecha = '';
					$hora = '';

					$personal_id = $busca_personal[0]['id'];

					while($row=mysqli_fetch_array($genera_fecha))
					{ 
						$fecha = $row['CURDATE()'] ;
					}
					while($row=mysqli_fetch_array($genera_hora))
					{ 
						$hora = $row['NOW()'] ;
					}

					$foto_personal = './views/personal/fotos/'.$busca_personal[0]['foto'];
					//enviar post alumno_id
					$consulta_insercion = "INSERT INTO registro(codigo_barras,alumno_id,personal_id,fecha,hora) VALUES('$codigo_barras',$alumno_id,'$personal_id','$fecha','$hora')";
					$link->query($consulta_insercion);

					if($link->error){echo $link->error;}
					else{
						header('Content-type: application/json; charset=utf-8');
						echo json_encode(array("resultado"=>true,"alumno_id"=>$personal_id,"foto_registro"=>$foto_personal,"fotos"=>array('0' => array('foto' => './views/img/correcto.png','nombre' => '','apellido_paterno' => '','apellido_materno' => '' ) )));
						exit();
					}
					
				}
				else{
					header('Content-type: application/json; charset=utf-8');
					echo json_encode(array("resultado"=>false));
					exit();
				}
			}
			else{
				header('Content-type: application/json; charset=utf-8');
				echo json_encode(array("resultado"=>false));
				exit();
			}

		*/
		
	}

	public function reset(){

		$conexion = new Conexion();
		$conexion->selecciona_base_datos();
		$link = $conexion->link;

		$result = $link->query("SELECT *  FROM registro  WHERE fecha = CURDATE() ORDER BY hora");

		if($result->num_rows == 0){
			$archivo_ninos = "contador_ninos.txt";
			$fp_ninos = fopen($archivo_ninos,"w+");
			fwrite($fp_ninos, 0, 26);
			fclose($fp_ninos);
			$archivo_ninas = "contador_ninas.txt";
			$fp_ninas = fopen($archivo_ninas,"w+");
			fwrite($fp_ninas, 0, 26);
			fclose($fp_ninas);
			$archivo_personal = "contador_personal.txt";
			$fp_personal = fopen($archivo_personal,"w+");
			fwrite($fp_personal, 0, 26);
			fclose($fp_personal);
		}
	}
	
}
?>
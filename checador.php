<?php
if(!isset($_GET['seccion'])){
	$seccion = '';
}
else{
	$seccion = $_GET['seccion'];
}
if(!isset($_GET['accion'])){
	$accion = '';
}
else{
	$accion = $_GET['accion'];
}

if (!empty($seccion)) {
	require_once('Controladores/controlador_'.$seccion.'.php');	

	$name_ctl = 'controlador_'.$seccion;
	$controlador = new $name_ctl;
	if($accion == 'alta_bd'){
		$controlador->alta_bd();
	}
}elseif(empty($accion) && empty($seccion)){
	require_once('Controladores/controlador_registro.php');	

	$name_ctl = 'controlador_registro';
	$controlador = new $name_ctl;
	if($accion == ''){
		$controlador->reset();
	}
}

$archivo_ninos = "contador_ninos.txt";
$fp_ninos = fopen($archivo_ninos,"r");
$contador_ninos = fgets($fp_ninos, 26);
fclose($fp_ninos);
$archivo_ninas = "contador_ninas.txt";
$fp_ninas = fopen($archivo_ninas,"r");
$contador_ninas = fgets($fp_ninas, 26);
fclose($fp_ninas);
$archivo_personal = "contador_personal.txt";
$fp_personal = fopen($archivo_personal,"r");
$contador_personal = fgets($fp_personal, 26);
fclose($fp_personal);

?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Registro</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">

		<link rel="stylesheet" href="./views/css/bootstrap.min.css">
  		<link rel="stylesheet" href="./views/css/bootstrap-theme.min.css">
  		<link rel="stylesheet" href="./views/css/layout.css">

  		<script src="./views/js/jquery.min.js"></script>	
  		<script src="./views/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="./views/js/funciones.js"></script>

	</head>
	<body style="background-color: #0073AB;">

		<div class="row">
			<div class="col-md-5 text-center">
				<img  class="logo_checador" src="./views/img/logo_blanco.svg" >
			</div>
			<div class="col-md-2 text-center" id="contador-ninos"><?php echo $contador_ninos; ?></div>

			<div class="col-md-2 text-center" id="contador-ninas"><?php echo $contador_ninas; ?></div>

			<div class="col-md-2 text-center" id="contador-personal"><?php echo $contador_personal; ?></div>
		</div>
		
			
		<a href="" id="next" data-toggle="modal" data-target="#ventanaEditar" data-backdrop="static" title="Presione para registrar manualmente">
			<div class="clock col-md-12 text-center" id="hora">
				
				<span id="hours"></span>
				<span id="point">:</span>
				<span id="min"></span>
				<span id="point">:</span>
				<span id="sec"></span>
				
			</div>
		</a>
		<div id="Date" class="col-md-12 text-center"></div>

		<form id="form-entrada-salida-responsable" name="form-entrada-salida-responsable" 
		method="post" 
		action="./index.php?seccion=registro_entrada_salida&accion=alta_bd">
			
			<input autofocus type='text' id='codigo_barras_entrada' name='codigo_barras_entrada' style="background-color:transparent;color:transparent;border-width:0;">
			<input type='text' id='codigo_barras_responsable' name='codigo_barras_responsable' style="background-color:transparent;color:transparent;border-width:0;">
			<input type='hidden' id='alumno_id' name='alumno_id'>
		</form>
		<?php 
			include('./views/zoom_registro.php');
			include('./views/zoom_error.php');
		?>
	</body>
</html>

<script>

	$(document).ready(function() {
		//var contador = 0;
		var consulta;
		var codigo_barras;
		var operacion_contador;
		var genero;
		//$("#contador").html(contador);
		$("#codigo_barras_entrada").on('keyup', function (e) {
		    if (e.keyCode == 13) {
		    	
           		var codigo_barras_entrada = document.getElementById("codigo_barras_entrada").value;
	            $.ajax({
	                url: "./checador.php?seccion=registro&accion=alta_bd",
	                type: "POST",
	                data: {
	                	accion: 'Entrada',
	                    codigo_barras: codigo_barras_entrada
	                },
	                dataType:'JSON',
	                success: function(response) {

	                	if(response.existe){
	                		var foto_registro='<img src="'+response.foto_registro+'" class="zoom_img">';
							var fotos='';
							consulta = response.consulta;
							genero = response.genero;

							for(var i=0;i<response.fotos.length;i++){

								if (i==0) {
									fotos += '<div class="item active">';
									fotos += '<img class="img-responsive" src="'+response.fotos[i]['foto']+'" alt="...">';
									fotos += '<div class="carousel-caption">';
									fotos += '<h1>'+response.fotos[i]['nombre'] + ' ' + response.fotos[i]['apellido_paterno'] + ' ' + response.fotos[i]['apellido_materno']+'</h1>';
									fotos += '</div>';
									fotos += '</div>';
								}
								else{
									fotos += '<div class="item">';
									fotos += '<img class="img-responsive" src="'+response.fotos[i]['foto']+'" alt="...">';
									fotos += '<div class="carousel-caption">';
									fotos += '<h1>'+response.fotos[i]['nombre'] + ' ' + response.fotos[i]['apellido_paterno'] + ' ' + response.fotos[i]['apellido_materno']+'</h1>';
									fotos += '</div>';
									fotos += '</div>';
								}
							}

							$("#foto_registro").html(foto_registro);
							$("#fotos").html(fotos);
							
		                	$('#alumno_id').val(response.id);
		                	$('#zoom_registro').modal('show');

		                	$("#contador").html(response.contador);
		                	
		                	setTimeout(function() {
						        $('#zoom_registro').modal('hide');
						        $('#fotos').html('');
						        if (response.accion_realizada == 'Recargar') {
						        	location.reload(true);
						        }
						        if(response.id != ''){
									document.getElementById("codigo_barras_responsable").focus();
									codigo_barras = response.codigo_barras;
									operacion_contador = response.operacion_contador;
									//$('#codigo_barras_responsable').val(codigo_barras);
								}
						    },5000);
		                }
		                else{
		                	$('#zoom_error').modal('show');
		                	setTimeout(function() {
						        $('#zoom_error').modal('hide');
						        location.reload(true);
						    },5000);
		                }
	                },
	                error: function(xhr, status) {
		                location.reload(true);
	                }
	            });

		    }
		});
		$("#codigo_barras_responsable").on('keyup', function (e) {
		    if (e.keyCode == 13) {
		    	
           		var codigo_barras_responsable = document.getElementById("codigo_barras_responsable").value;
           		var codigo_barras_alumno = codigo_barras;
           		var alumno_id = document.getElementById("alumno_id").value;
           		//var codigo_barras_entrada = document.getElementById("codigo_barras_entrada").value;
	            $.ajax({
	                url: "./checador.php?seccion=registro&accion=alta_bd",
	                type: "POST",
	                data: {
	                    codigo_barras_alumno: codigo_barras_alumno,
	                    codigo_barras: codigo_barras_responsable,
	                    alumno_id: alumno_id,
	                    consulta: consulta,
	                    accion: 'Responsable',
	                    genero: genero,
	                    operacion_contador: operacion_contador
	                },
	                dataType:'JSON',
	                success: function(response) {

	                	if(response.existe){
	                		var foto_registro='<img src="'+response.foto_registro+'" class="zoom_img">';
							var fotos='';

							for(var i=0;i<response.fotos.length;i++){

								if (i==0) {
									fotos += '<div class="item active">';
									fotos += '<img class="img-responsive" src="'+response.fotos[i]['foto']+'" alt="...">';
									fotos += '<div class="carousel-caption">';
									fotos += '<h1>'+response.fotos[i]['nombre'] + ' ' + response.fotos[i]['apellido_paterno'] + ' ' + response.fotos[i]['apellido_materno']+'</h1>';
									fotos += '</div>';
									fotos += '</div>';
								}
								else{
									fotos += '<div class="item">';
									fotos += '<img class="img-responsive" src="'+response.fotos[i]['foto']+'" alt="...">';
									fotos += '<div class="carousel-caption">';
									fotos += '<h1>'+response.fotos[i]['nombre'] + ' ' + response.fotos[i]['apellido_paterno'] + ' ' + response.fotos[i]['apellido_materno']+'</h1>';
									fotos += '</div>';
									fotos += '</div>';
								}
							}

							$("#foto_registro").html(foto_registro);
							$("#fotos").html(fotos);
		                	$('#zoom_registro').modal('show');
		                	//$("#contador").html(response.contador);
		                	//$('#carousel_registro').carousel();
		                	setTimeout(function() {
						        $('#zoom_registro').modal('hide');
						        //$('#fotos').removeData();
						        $('#fotos').html('');
						        location.reload(true);
						    },5000);
		                }
		                else{
		                	$('#zoom_error').modal('show');
		                	setTimeout(function() {
						        $('#zoom_error').modal('hide');
						        location.reload(true);
						    },5000);
		                }
	                },
	                error: function(xhr, status) {
		                location.reload(true);
	                }
	            });

		    }
		});



		// Create two variable with the names of the months and days in an array
		var monthNames = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"]; 
		var dayNames= ["Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado"];

		// Create a newDate() object
		var newDate = new Date();
		// Extract the current date from Date object
		newDate.setDate(newDate.getDate());
		// Output the day, date, month and year   
		$('#Date').html(dayNames[newDate.getDay()] + " " + newDate.getDate() + ' ' + monthNames[newDate.getMonth()] + ' ' + newDate.getFullYear());

		setInterval( function() {
			// Create a newDate() object and extract the seconds of the current time on the visitor's
			var seconds = new Date().getSeconds();
			// Add a leading zero to seconds value
			$("#sec").html(( seconds < 10 ? "0" : "" ) + seconds);
			},1000);
			
		setInterval( function() {
			// Create a newDate() object and extract the minutes of the current time on the visitor's
			var minutes = new Date().getMinutes();
			// Add a leading zero to the minutes value
			$("#min").html(( minutes < 10 ? "0" : "" ) + minutes);
		    },1000);
			
		setInterval( function() {
			// Create a newDate() object and extract the hours of the current time on the visitor's
			var hours = new Date().getHours();
			// Add a leading zero to the hours value
			$("#hours").html(( hours < 10 ? "0" : "" ) + hours);
		    }, 1000);	
	});

</script>

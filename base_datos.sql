DROP TABLE accion_grupo;
DROP TABLE asigna_madre_padre_tutor;
DROP TABLE asigna_persona_autorizada;
DROP TABLE registro;
DROP TABLE usuario;
DROP TABLE accion;
DROP TABLE grupo;
DROP TABLE seccion;
DROP TABLE madre_padre_tutor;
DROP TABLE persona_autorizada;
DROP TABLE token;
DROP TABLE alumno;
DROP TABLE personal;



CREATE TABLE  grupo(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    descripcion VARCHAR(500) NOT NULL UNIQUE,
    observaciones TEXT,
    status TINYINT(1) NOT NULL)ENGINE=InnoDB;


CREATE TABLE seccion(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    descripcion VARCHAR(500) NOT NULL UNIQUE,
    observaciones TEXT,
    status TINYINT(1) NOT NULL
    )ENGINE=InnoDB;

CREATE TABLE  usuario(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    user VARCHAR(500) NOT NULL UNIQUE,
    password VARCHAR(50) NOT NULL UNIQUE,
    email VARCHAR(500) NOT NULL UNIQUE,
    grupo_id INT(11) NOT NULL,
    FOREIGN KEY (grupo_id) REFERENCES grupo(id)
    ON UPDATE CASCADE ON DELETE RESTRICT
    )ENGINE=InnoDB;

CREATE TABLE  accion(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    descripcion VARCHAR(500) NOT NULL UNIQUE,
    seccion_id INT(11) NOT NULL,
    FOREIGN KEY (seccion_id) REFERENCES seccion(id)
    ON UPDATE CASCADE ON DELETE RESTRICT
    )ENGINE=InnoDB;


CREATE TABLE  accion_grupo(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    accion_id INT(11) NOT NULL,
    grupo_id INT(11) NOT NULL,
    FOREIGN KEY (accion_id) REFERENCES accion(id)
    ON UPDATE CASCADE ON DELETE RESTRICT,
    FOREIGN KEY (grupo_id) REFERENCES grupo(id)
    ON UPDATE CASCADE ON DELETE RESTRICT
    )ENGINE=InnoDB;

ALTER TABLE accion_grupo ADD UNIQUE INDEX accion_grupo (accion_id, grupo_id);

CREATE TABLE  token(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    usuario_id INT(11) NOT NULL,
    grupo_id INT(11) NOT NULL,
    nombre_usuario VARCHAR(500) NOT NULL,
    token TEXT NOT NULL
    )ENGINE=InnoDB;

CREATE TABLE  madre_padre_tutor(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(500) NOT NULL,
    apellido_paterno VARCHAR(500) NOT NULL,
    apellido_materno VARCHAR(500) NOT NULL,
    fecha_nacimiento DATE NOT NULL,
    foto LONGBLOB,
    observaciones TEXT,
    status TINYINT(1) NOT NULL,
    telefono VARCHAR(15) NOT NULL,
    codigo_barras VARCHAR(20) NOT NULL,
    domicilio TEXT NOT NULL
    )ENGINE=InnoDB;

    

CREATE TABLE  persona_autorizada(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(500) NOT NULL,
    apellido_paterno VARCHAR(500) NOT NULL,
    apellido_materno VARCHAR(500) NOT NULL,
    fecha_nacimiento DATE NOT NULL,
    foto LONGBLOB,
    observaciones TEXT,
    status TINYINT(1) NOT NULL,
    telefono VARCHAR(15) NOT NULL,
    codigo_barras VARCHAR(20) NOT NULL,
    domicilio TEXT NOT NULL
    )ENGINE=InnoDB;


CREATE TABLE  alumno(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(500) NOT NULL,
    apellido_paterno VARCHAR(500) NOT NULL,
    apellido_materno VARCHAR(500) NOT NULL,
    fecha_nacimiento DATE NOT NULL,
    foto LONGBLOB,
    observaciones TEXT,
    status TINYINT(1) NOT NULL,
    codigo_barras VARCHAR(20) NOT NULL,
    tipo_sangre VARCHAR(15) NOT NULL,
    genero VARCHAR(20) NOT NULL,
    lugar_nacimiento TEXT NOT NULL,
    fecha_ingreso DATE NOT NULL
    )ENGINE=InnoDB;

CREATE TABLE  asigna_madre_padre_tutor(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    alumno_id INT(11) NOT NULL,
    madre_padre_tutor_id INT(11) NOT NULL,
    FOREIGN KEY (alumno_id) REFERENCES alumno(id)
    ON UPDATE CASCADE ON DELETE RESTRICT,
    FOREIGN KEY (madre_padre_tutor_id) REFERENCES madre_padre_tutor(id)
    ON UPDATE CASCADE ON DELETE RESTRICT
    )ENGINE=InnoDB;

ALTER TABLE asigna_madre_padre_tutor 
ADD UNIQUE INDEX asigna_madre_padre_tutor (alumno_id, madre_padre_tutor_id);

CREATE TABLE  asigna_persona_autorizada(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    alumno_id INT(11) NOT NULL,
    persona_autorizada_id INT(11) NOT NULL,
    FOREIGN KEY (alumno_id) REFERENCES alumno(id)
    ON UPDATE CASCADE ON DELETE RESTRICT,
    FOREIGN KEY (persona_autorizada_id) REFERENCES persona_autorizada(id)
    ON UPDATE CASCADE ON DELETE RESTRICT
    )ENGINE=InnoDB;

ALTER TABLE asigna_persona_autorizada
ADD UNIQUE INDEX asigna_persona_autorizada (alumno_id, persona_autorizada_id);

CREATE TABLE  personal(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(500) NOT NULL,
    apellido_paterno VARCHAR(500) NOT NULL,
    apellido_materno VARCHAR(500) NOT NULL,
    fecha_nacimiento DATE NOT NULL,
    foto LONGBLOB,
    observaciones TEXT,
    status TINYINT(1) NOT NULL,
    telefono VARCHAR(15) NOT NULL,
    puesto VARCHAR(50) NOT NULL,
    codigo_barras VARCHAR(20) NOT NULL,
    domicilio TEXT NOT NULL,
    horario TEXT NOT NULL,
    area TEXT NOT NULL
    )ENGINE=InnoDB;

CREATE TABLE registro(
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    codigo_barras VARCHAR(20) NOT NULL,
    alumno_id INT(11),
    madre_padre_tutor_id INT(11),
    persona_autorizada_id INT(11),
    personal_id INT(11),
    fecha DATE NOT NULL,
    hora DATETIME NOT NULL,
    FOREIGN KEY (alumno_id) REFERENCES alumno(id)
    ON UPDATE CASCADE ON DELETE RESTRICT,
    FOREIGN KEY (madre_padre_tutor_id) REFERENCES madre_padre_tutor(id)
    ON UPDATE CASCADE ON DELETE RESTRICT,
    FOREIGN KEY (persona_autorizada_id) REFERENCES persona_autorizada(id)
    ON UPDATE CASCADE ON DELETE RESTRICT,
    FOREIGN KEY (personal_id) REFERENCES personal(id)
    ON UPDATE CASCADE ON DELETE RESTRICT
    )ENGINE=InnoDB;


INSERT INTO grupo (descripcion, observaciones, status) VALUES ('Admins','Admins',1);
INSERT INTO usuario (user,password,email,grupo_id) VALUES('root','moro58','mgamboa@tdesystems.com.mx', 1);
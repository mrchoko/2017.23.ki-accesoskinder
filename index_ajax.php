<?php 
session_start();
$now = time();
if(isset($_SESSION['loggedin'])){
	if($_SESSION['loggedin']==True){
		$menu = True;
	}

}

if (isset($_SESSION['expire'])){
	$expire = $_SESSION['expire'];
}
else{
	$expire = False;	
	$menu = False;
}
if( $now > $expire || ( !isset($_SESSION['loggedin']) && (!$_SESSION['loggedin'] == true) ) ) {
	session_destroy();
	$_GET['seccion'] = 'session';
	$_GET['accion'] = 'login';
	$menu = False;
}
if(!isset($_GET['seccion'])){
	$seccion='session';
	$_GET['seccion'] = 'session';
	$_GET['accion'] = 'login';
}
else{
	$seccion = $_GET['seccion'];
}
if(!isset($_GET['accion'])){
	$accion='login';	
}
else{
	$accion = $_GET['accion'];
}
if(!isset($_GET['resultado'])){
	$resultado='';	
}
else{
	$resultado = $_GET['resultado'];
}
if(!isset($_GET['operacion'])){
	$operacion='';	
}
else{
	$operacion = $_GET['operacion'];
}
if(!isset($_GET['token'])){
	$token='';	
}
else{
	$token = $_GET['token'];
}
require_once('./Controladores/controlador_'.$seccion.'.php');	
?>

<!DOCTYPE html>
<html>

	<head>
		<script type="text/javascript" src="./views/js/funciones.js"></script>
	</head>
	<body>
		<?php include('./views/'.$seccion.'/'.$accion.'.php'); ?>
	</body>
</html>
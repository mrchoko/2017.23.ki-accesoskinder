<?php 
session_start();
$now = time();
if(isset($_SESSION['loggedin'])){
	if($_SESSION['loggedin']==True){
		$menu = True;
	}

}

if (isset($_SESSION['expire'])){
	$expire = $_SESSION['expire'];
}
else{
	$expire = False;	
	$menu = False;
}
if( $now > $expire || ( !isset($_SESSION['loggedin']) && (!$_SESSION['loggedin'] == true) ) ) {
	session_destroy();
	$_GET['seccion'] = 'session';
	$_GET['accion'] = 'login';
	$menu = False;
}
if(!isset($_GET['seccion'])){
	$seccion='session';
	$_GET['seccion'] = 'session';
	$_GET['accion'] = 'login';
}
else{
	$seccion = $_GET['seccion'];
}
if(!isset($_GET['accion'])){
	$accion='login';	
}
else{
	$accion = $_GET['accion'];
}
if(!isset($_GET['resultado'])){
	$resultado='';	
}
else{
	$resultado = $_GET['resultado'];
}
if(!isset($_GET['operacion'])){
	$operacion='';	
}
else{
	$operacion = $_GET['operacion'];
}
if(!isset($_GET['token'])){
	$token='';	
}
else{
	$token = $_GET['token'];
}
if ($seccion !== 'credencial') {
	require_once('Controladores/controlador_'.$seccion.'.php');	

	$name_ctl = 'controlador_'.$_GET['seccion'];
	$controlador = new $name_ctl;
	if($accion == 'alta_bd'){
		$controlador->alta_bd();
	}

	if($accion == 'elimina'){
		$controlador->elimina_bd();		
	}
}
else{
	require_once('Controladores/controlador_'.$_GET['tabla'].'.php');	
}
require_once('views/mensaje.php');

?>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8" name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, target-densityDpi=device-dpi">
		<title>Control de accesos</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" href="./views/css/bootstrap.min.css">
  		<link rel="stylesheet" href="./views/css/bootstrap-theme.min.css">
  		<link rel="stylesheet" href="./views/css/layout.css">
		<link rel="stylesheet" href="./views/css/layout.css" media="print">

  		<link href="./views/css/bootstrap-datetimepicker.min.css" rel="stylesheet">

  		<script src="./views/js/jquery.min.js"></script>	
  		<script src="./views/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="./views/js/funciones.js"></script>


		<script type='text/javascript'>
			$(document).ready(function () {
			<?php if(!empty($operacion) && !empty($resultado)) { ?>
				$('#<?php echo $resultado.''.$operacion; ?>').modal('show'); 
			<?php } ?>
			});
		</script>  
	</head>
	<body>
		<div id='encabezado' class="row center-aligned">
			<div class="col-md-2">
				<img  class="logo" src="./views/img/logo_blanco.svg">
			</div>
			<div class="col-md-10 text-center">
				<h1 style="color: #FFFFFF;">Control de Accesos Escolar</h1>	
			</div>
		</div>

		<?php 
		if($menu){
		?>

		<div class="row affix-row">
			<div id="menu_responsivo" class="col-md-2 affix-sidebar">
				<div class="sidebar-nav">
					<div class="navbar navbar-default" role="navigation">
						<div class="navbar-header">
						    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse" >
							    <span class="sr-only"></span>
							    <span class="icon-bar"></span>
							    <span class="icon-bar"></span>
							    <span class="icon-bar"></span>
						    </button>
						    <span class="visible-xs navbar-brand">Menu</span>
					    </div>
						<div class="navbar-collapse collapse sidebar-navbar-collapse well" id="nav-main">
							<ul class="nav navbar-nav scroll-menu" id="menu" >

								<li class="active">
									<a data-toggle="tab" href="#" ><h4>Panel de Control<br></h4></a>
								</li>
								<li>
									<a data-toggle="collapse" data-target="#grupo" data-parent="#menu" class="collapsed puntero" >
										<span class="glyphicon glyphicon-cloud"></span> Grupo <span class="caret pull-right"></span>
									</a>
									<div class="collapse" id="grupo" style="height: 0px;" >
										<ul class="nav nav-list">
											<li><a href="index.php?seccion=grupo&accion=alta"><p>Alta</p></a></li>
											<li><a href="index.php?seccion=grupo&accion=lista"><p>Lista</p></a></li>
										</ul>
									</div>
								</li>
								<li>
									<a data-toggle="collapse" data-target="#seccion" data-parent="#menu" class="collapsed puntero" >
										<span class="glyphicon glyphicon-inbox"></span> Sección <span class="caret pull-right"></span>
									</a>
									<div class="collapse" id="seccion" style="height: 0px;">
										<ul class="nav nav-list">
											<li><a href="index.php?seccion=seccion&accion=alta"><p>Alta</p></a></li>
											<li><a href="index.php?seccion=seccion&accion=lista"><p>Lista</p></a></li>
										</ul>
									</div>
								</li>
								<li>
									<a data-toggle="collapse" data-target="#usuario" data-parent="#menu" class="collapsed puntero">
										<span class="glyphicon glyphicon-lock"></span> Usuario <span class="caret pull-right"></span>
									</a>
									<div class="collapse" id="usuario" style="height: 0px;">
										<ul class="nav nav-list">
											<li><a href="index.php?seccion=usuario&accion=alta"><p>Alta</p></a></li>
											<li><a href="index.php?seccion=usuario&accion=lista"><p>Lista</p></a></li>
										</ul>
									</div>
								</li>
								<li>
									<a data-toggle="collapse" data-target="#accion" data-parent="#menu" class="collapsed puntero">
										<span class="glyphicon glyphicon-wrench"></span> Acción <span class="caret pull-right"></span>
									</a>
									<div class="collapse" id="accion" style="height: 0px;">
										<ul class="nav nav-list">
											<li><a href="index.php?seccion=accion&accion=alta"><p>Alta</p></a></li>
											<li><a href="index.php?seccion=accion&accion=lista"><p>Lista</p></a></li>
										</ul>
									</div>
								</li>
								<li>
									<a data-toggle="collapse" data-target="#accion_grupo" data-parent="#menu" class="collapsed puntero">
										<span class="glyphicon glyphicon-cog"></span> Acción Grupo <span class="caret pull-right"></span>
									</a>
									<div class="collapse" id="accion_grupo" style="height: 0px;">
										<ul class="nav nav-list">
											<li><a href="index.php?seccion=accion_grupo&accion=alta"><p>Alta</p></a></li>
											<li><a href="index.php?seccion=accion_grupo&accion=lista"><p>Lista</p></a></li>
										</ul>
									</div>
								</li>
								<li>
									<a data-toggle="collapse" data-target="#token" data-parent="#menu" class="collapsed puntero">
										<span class="glyphicon glyphicon-cog"></span> Token <span class="caret pull-right"></span>
									</a>
									<div class="collapse" id="token" style="height: 0px;">
										<ul class="nav nav-list">
											<li><a href="index.php?seccion=token&accion=lista&token=<?php echo $token; ?>"><p>Lista</p></a></li>
										</ul>
									</div>
								</li>
								<li>
									<a data-toggle="collapse" data-target="#madre_padre_tutor" data-parent="#menu" class="collapsed puntero">
										<span class="glyphicon glyphicon-cog"></span> MPT <span class="caret pull-right"></span>
									</a>
									<div class="collapse" id="madre_padre_tutor" style="height: 0px;">
										<ul class="nav nav-list">
											<li><a href="index.php?seccion=madre_padre_tutor&accion=alta"><p>Alta</p></a></li>
											<li><a href="index.php?seccion=madre_padre_tutor&accion=lista"><p>Lista</p></a></li>
										</ul>
									</div>
								</li>
								<li>
									<a data-toggle="collapse" data-target="#persona_autorizada" data-parent="#menu" class="collapsed puntero">
										<span class="glyphicon glyphicon-cog"></span> Persona Autorizada<span class="caret pull-right"></span>
									</a>
									<div class="collapse" id="persona_autorizada" style="height: 0px;">
										<ul class="nav nav-list">
											<li><a href="index.php?seccion=persona_autorizada&accion=alta"><p>Alta</p></a></li>
											<li><a href="index.php?seccion=persona_autorizada&accion=lista"><p>Lista</p></a></li>
										</ul>
									</div>
								</li>
								<li>
									<a data-toggle="collapse" data-target="#alumno" data-parent="#menu" class="collapsed puntero">
										<span class="glyphicon glyphicon-cog"></span> Alumno<span class="caret pull-right"></span>
									</a>
									<div class="collapse" id="alumno" style="height: 0px;">
										<ul class="nav nav-list">
											<li><a href="index.php?seccion=alumno&accion=alta"><p>Alta</p></a></li>
											<li><a href="index.php?seccion=alumno&accion=lista"><p>Lista</p></a></li>
										</ul>
									</div>
								</li>
								<li>
									<a data-toggle="collapse" data-target="#personal" data-parent="#menu" class="collapsed puntero">
										<span class="glyphicon glyphicon-cog"></span> Personal<span class="caret pull-right"></span>
									</a>
									<div class="collapse" id="personal" style="height: 0px;">
										<ul class="nav nav-list">
											<li><a href="index.php?seccion=personal&accion=alta"><p>Alta</p></a></li>
											<li><a href="index.php?seccion=personal&accion=lista"><p>Lista</p></a></li>
										</ul>
									</div>
								</li>
								<li>
									<a data-toggle="collapse" data-target="#reportes" data-parent="#menu" class="collapsed puntero">
										<span class="glyphicon glyphicon-cog"></span> Reportes<span class="caret pull-right"></span>
									</a>
									<div class="collapse" id="reportes" style="height: 0px;">
										<ul class="nav nav-list">
											<li><a href="index.php?seccion=reportes&accion=accesos"><p>Accesos</p></a></li>
											<li><a href="index.php?seccion=reportes&accion=inscripcion"><p>Inscripción</p></a></li>
										</ul>
									</div>
								</li>
								<li>
									<a data-toggle="collapse" data-target="#session" data-parent="#menu" class="collapsed puntero">
										<span class="glyphicon glyphicon-cog"></span> Sesión <span class="caret pull-right"></span>
									</a>
									<div class="collapse" id="session" style="height: 0px;">
										<ul class="nav nav-list">
											<li><a href="index.php?seccion=session&accion=logout"><p>Cerrar Sesión</p></a></li>
										</ul>
									</div>
								</li>
								
							</ul>
						</div>
					</div>

				</div>
			</div>

			<?php 
			}

			if($menu){
				$n_cols_principal = 10;
			}
			else{
				$n_cols_principal = 12;	
			}
			?>

			<div class="col-md-<?php echo $n_cols_principal; ?> affix-content">
				<div class="container">
					<div class="tab-content">
						<div id="home" class="tab-pane fade in active">
							<div class="page-header text-center col-md-12">
								<h3 class='border-bottom col-md-12'><?php echo $seccion.' / '.$accion; ?></h3>
								<?php 
									include('./views/'.$seccion.'/'.$accion.'.php'); 
								?>
							</div>	
						</div>
					</div>
				</div>
			</div>

		<?php $mensaje_controller->genera_mensaje($resultado,$operacion); ?>

		<?php 
			include('./views/zoom_foto.php');
		?>
		<link href="./views/css/bootstrap-select.min.css" rel="stylesheet"/>
		<script src="./views/js/bootstrap-select.min.js"></script>
  		<link rel="stylesheet" href="./views/css/bootstrap-formhelpers.min.css">
  		<script src="./views/js/bootstrap-formhelpers.min.js"></script>
  		<link rel="stylesheet" href="./views/css/clockface.css">
  		<script src="./views/js/clockface.js"></script>  		
		<script src="./views/js/moment.min.js"></script>
		<script src="./views/js/bootstrap-datetimepicker.min.js"></script>
		<script src="./views/js/bootstrap-datetimepicker.es.js"></script>
		<script src="./views/js/printThis.js"></script>
		<script type="text/javascript">
		    $('#divMiCalendario').datetimepicker({
		        format: 'YYYY-MM-DD',
		        autoclose: true     
		    });
		    $('#calendario_inicial,#calendario_final,#calendario_fecha_nacimiento,#calendario_fecha_ingreso').datetimepicker({
		        format: 'YYYY-MM-DD',
		        autoclose: true     
		    });
		    $('#hora_entrada,#hora_salida').clockface({format: 'HH:mm'});
		</script>
	</body>
</html>
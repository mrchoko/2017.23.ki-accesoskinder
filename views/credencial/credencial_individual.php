</h3></div></div>

<?php if ($_GET['tabla'] == 'alumno') { ?>

<div id="contenido">
	<div class="row" style="margin: 10px 10px 10px 10px;">
		<div id='credencial_frente_alumno' class="col-xs-6">
			<div class="row">
				&nbsp;
			</div>
			<div class="row">
				<p id="nombre_estancia_credencial_alumno">ESTANCIA INFANTIL Y PREESCOLAR "FRIDA KAHLO"</p>
			</div>
			<div class="row">
				&nbsp;
			</div>
        	<div class="form-group row">
        		<div class="col-xs-6">
		        	<img src="./views/img/logo_color.svg" class="logo_credencial" id="logo_credencial_alumno">
        		</div>
        		<div class="col-xs-6">
		        	<img src="<?php echo './views/'.$_GET['tabla'].'/fotos/'.$_GET['foto'] ?>" class="img_perfil" width="120" height="120">
        		</div>
        	</div>
        	<div class="row">
				&nbsp;
			</div>
			<div class="row">
				&nbsp;
			</div>
        	<div class="form-group row">
	  			<div class="col-md-12">
	  				<textarea style="font: sans-serif;font-size: 25px;text-align: center;" class="form-control noresize" rows="2" disabled><?php echo $_GET['nombre']; ?></textarea>
	  			</div>
	  		</div>
	  		<div class="row">
				&nbsp;
			</div>
        </div>
        <div id='credencial_espalda_alumno' class="col-xs-6" >
        	<div class="row">
				&nbsp;
			</div>
			<div class="row">
				&nbsp;
			</div>
        	<div class="form-group row">
	  			<div class="col-md-12">
	  				<textarea style="font: sans-serif;font-size: 25px;" class="form-control noresize" name="direccion_completo" rows="3" disabled>Privada Ruiz Cortines #34 Col. El Zapote, Tonalá, Jal.</textarea>
	  			</div>
	  		</div>
			<div class="row">
				&nbsp;
			</div>
			<div class="row">
				&nbsp;
			</div>
			<div class="row">
				&nbsp;
			</div>
			<div class="row">
				<div class="col-md-12 text-center">
		        	<p style="text-align:center;font-size:30px;">Firma Director</p>
		  		</div>
			</div>
			<div class="row">
				&nbsp;
			</div>
			<div class="row">
				&nbsp;
			</div>
			<div class="row">
				&nbsp;
			</div>
			<div class="row">
				&nbsp;
			</div>
	       	<div class="form-group row">
				<div class="col-md-12 text-center">
		        	<img src="<?php echo './views/'.$_GET['tabla'].'/codigo_barras/'.$_GET['credencial'].'.jpg' ?>" class="img_codigo_barras" id="img_codigo_barras" >
		  		</div>
	   		</div>

		</div>
	</div>
</div>

<?php }else{ ?>

<div id="contenido">
	<div class="row" style="margin: 10px 10px 10px 10px;">
		<div id='credencial_frente' class="col-xs-6">
			<div class="row">
				&nbsp;
			</div>
        	<div class="form-group row">
        		<div class="col-xs-4">
		        	<img src="./views/img/logo_color.svg" class="logo_credencial" id="logo_credencial">
        		</div>
	       		<div class="col-xs-4">
        			<p style="text-align:center;font-size:10px;font: sans-serif;">ESTANCIA INFANTIL<br>Y PREESCOLAR<br>"FRIDA KAHLO"</p>
        		</div>
        		<div class="col-xs-4">
		        	<img src="<?php echo './views/'.$_GET['tabla'].'/fotos/'.$_GET['foto'] ?>" class="img_perfil" width="80" height="80">
        		</div>
        	</div>
        	<div class="form-group row">
	  			<div class="col-md-12">
	  				<textarea style="font: sans-serif;" class="form-control noresize" rows="2" disabled><?php echo $_GET['nombre']; ?></textarea>
	  			</div>

	  		</div>

        </div>
        <div id='credencial_espalda' class="col-xs-6" >
        	<div class="row">
				&nbsp;
			</div>
        	<div class="form-group row">
	  			<div class="col-md-12">
	  				<textarea style="font: sans-serif;" class="form-control noresize" name="direccion_completo" rows="2" disabled>Privada Ruiz Cortines #34 Col. El Zapote, Tonalá, Jal.</textarea>
	  			</div>

	  		</div>

	       	<div class="form-group row">
	       		<div class="col-md-4 text-center">
		  			<hr align="left" size="4" width="100%" color="black" />
		        	<p style="text-align:center;font-size:8px;font: 80% sans-serif;">Firma Director</p>
		  		</div>
		        <!--<div class="col-md-4 text-center">
		            <hr align="left" size="4" width="100%" color="black" />
			        	<p style="text-align:center;font-size:8px;font: 80% sans-serif;">Firma Alumno</p>
		        </div>-->
				<div class="col-md-8 text-center">
		        	<img src="<?php echo './views/'.$_GET['tabla'].'/codigo_barras/'.$_GET['credencial'].'.jpg' ?>" class="img_codigo_barras" id="img_codigo_barras" >
		  		</div>
	   		</div>

		</div>
	</div>
</div>

<?php } ?>
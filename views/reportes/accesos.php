	<form 
		id="form-reporte-generar" name="form-reporte-generar" 
		method="post" 
		action="./index.php?seccion=reportes&accion=reporte">
		<div class="row">&nbsp;</div>
		<div class="form-group row">
			<div class="col-md-4">
				<select name='filtro_reporte' class='selectpicker' data-live-search='true' 
				title='Seleccione Filtro' data-width='100%'
				data-none-results-text='No se encontraron resultados' required>
			        <option value='personal'>Personal</option>
			        <option value='alumno'>Alumno</option>
			    </select>
			</div>
			<div class="col-md-4">
				<div class='input-group date' id='calendario_inicial'>
                    <input type='text' id="fecha_inicial" name="fecha_inicial" class="form-control" placeholder="Ingresa Fecha Inicial" readonly/>
                    <span class="input-group-addon">
                    	<span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
			</div>
			<div class="col-md-4">
				<div class='input-group date' id='calendario_final'>
                    <input type='text' id="fecha_final" name="fecha_final" class="form-control" placeholder="Ingresa Fecha Final" readonly/>
                    <span class="input-group-addon">
                    	<span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
			</div>
		</div>
		<div class="form-group text-center row">
			<div class="col-md-12">
				<button type="submit" class="btn btn-secondary">Generar</button>
			</div>
		</div>
	</form>
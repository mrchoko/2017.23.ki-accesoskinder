<?php if ( !empty($registros) && $_POST['filtro_reporte'] == 'alumno' ) { 
  $contador=1; ?>
<div class="container" id='tabla_reporte'>           
  <table class="table table-striped">
    <thead>
      <tr>
        <th class="col-md-1">No.</th>
        <th class="col-md-3">Alumno</th>
        <th class="col-md-3">Responsable</th>
        <th class="col-md-2">Tipo Responsable</th>
        <th class="col-md-1">Fecha</th>
        <th class="col-md-2">Hora</th>
      </tr>
    </thead>
    <tbody>
    <?php foreach ($registros as $key => $registro) { ?>
      <tr id='fila_reporte'>
        <?php if (!empty($registro['madre_padre_tutor_id'])) { ?>
          
          <td class="col-md-1"><?php echo $contador; ?></td>
          <td class="col-md-3">
            <?php foreach ($alumnos as $key => $alumno) {
                if ($alumno['id'] == $registro['alumno_id']) {
                  echo $alumno['nombre'].' '.$alumno['apellido_paterno'].' '.$alumno['apellido_materno'];                
                }
              }?>
          </td>
          <td class="col-md-3">
            <?php foreach ($madres_padres_tutores as $key => $madre_padre_tutor) {
                if ($madre_padre_tutor['id'] == $registro['madre_padre_tutor_id']) {
                  echo $madre_padre_tutor['nombre'].' '.$madre_padre_tutor['apellido_paterno'].' '.$madre_padre_tutor['apellido_materno'];                
                }
              }?>
          </td>
          <td class="col-md-2">Madre, Padre o Tutor</td>
          <td class="col-md-1"><?php echo $registro['fecha']; ?></td>
          <td class="col-md-2"><?php echo $registro['hora']; ?></td>

        <?php $contador++; }elseif (!empty($registro['persona_autorizada_id'])) { ?>

          <td class="col-md-1"><?php echo $contador; ?></td>
          <td class="col-md-3">
            <?php foreach ($alumnos as $key => $alumno) {
                if ($alumno['id'] == $registro['alumno_id']) {
                  echo $alumno['nombre'].' '.$alumno['apellido_paterno'].' '.$alumno['apellido_materno'];                
                }
              }?>
          </td>
          <td class="col-md-3">
            <?php foreach ($personas_autorizadas as $key => $persona_autorizada) {
                if ($persona_autorizada['id'] == $registro['persona_autorizada_id']) {
                  echo $persona_autorizada['nombre'].' '.$persona_autorizada['apellido_paterno'].' '.$persona_autorizada['apellido_materno'];                
                }
              }?>
          </td>
          <td class="col-md-2">Persona Autorizada</td>
          <td class="col-md-1"><?php echo $registro['fecha']; ?></td>
          <td class="col-md-2"><?php echo $registro['hora']; ?></td>

        <?php $contador++; }elseif (!empty($registro['personal_id']) && !empty($registro['alumno_id'])) { ?>

          <td class="col-md-1"><?php echo $contador; ?></td>
          <td class="col-md-3">
            <?php foreach ($alumnos as $key => $alumno) {
                if ($alumno['id'] == $registro['alumno_id']) {
                  echo $alumno['nombre'].' '.$alumno['apellido_paterno'].' '.$alumno['apellido_materno'];                
                }
              }?>
          </td>
          <td class="col-md-3">
            <?php foreach ($personales as $key => $personal) {
                if ($personal['id'] == $registro['personal_id']) {
                  echo $personal['nombre'].' '.$personal['apellido_paterno'].' '.$personal['apellido_materno'];                
                }
              }?>
          </td>
          <td class="col-md-2">Personal</td>
          <td class="col-md-1"><?php echo $registro['fecha']; ?></td>
          <td class="col-md-2"><?php echo $registro['hora']; ?></td>

        <?php $contador++; }elseif (empty($registro['personal_id']) && empty($registro['persona_autorizada_id']) && empty($registro['madre_padre_tutor_id']) && !empty($registro['alumno_id'])){ ?>


        <?php } ?>
      </tr>
      <?php } ?>
    </tbody>
  </table>
</div>
<?php }elseif ( !empty($registros) && $_POST['filtro_reporte'] == 'personal' ) { $contador=1; ?>
<div class="container" id='tabla_reporte'>           
  <table class="table table-striped">
    <thead>
      <tr>
        <th class="col-md-1">No.</th>
        <th class="col-md-5">Personal</th>
        <th class="col-md-3">Puesto</th>
        <th class="col-md-1">Fecha</th>
        <th class="col-md-2">Hora</th>
      </tr>
    </thead>
    <tbody>
    <?php foreach ($registros as $key => $registro) { ?>
      <tr id='fila_reporte'>
        <?php if (!empty($registro['personal_id']) && empty($registro['persona_autorizada_id']) && empty($registro['madre_padre_tutor_id']) && empty($registro['alumno_id'])){ ?>

          <td class="col-md-1"><?php echo $contador; ?></td>
          <td class="col-md-5">
            <?php foreach ($personales as $key => $personal) {
                if ($personal['id'] == $registro['personal_id']) {
                  echo $personal['nombre'].' '.$personal['apellido_paterno'].' '.$personal['apellido_materno']; ?>
                  </td>
                  <td class="col-md-3"><?php echo $personal['puesto']; ?></td>
                <?php }
              }?>
          <td class="col-md-1"><?php echo $registro['fecha']; ?></td>
          <td class="col-md-2"><?php echo $registro['hora']; ?></td>

        <?php $contador++; } ?>
      </tr>
      <?php } ?>
    </tbody>
  </table>
</div>
<?php } ?>

  <!--
  <div class="form-group row">
    <div class="col-md-4 col-md-offset-4 text-center" style="border: 1px solid #000000;">
      <img  class="logo" src="./views/img/logo_color.svg">
    </div>
  </div>
  <div class="form-group row">
    <div class="col-md-4 col-md-offset-4 text-center">
      <h3 style="border: 1px solid #000000;"><b>FICHA DE INSCRIPCIÓN</b></h3>
    </div>
  </div>
  <div class="form-group row" style="border: 1px solid #000000;">
    <div class="col-md-4 col-md-offset-4 text-center">
      <h5><b>DATOS DEL MENOR</b></h5>
    </div>
  </div>
  <div class="form-group row" style="border: 1px solid #000000;">
    <div class="col-md-2">
      Nombre del niño(a):
    </div>
    <div class="col-md-5">
      <hr align="left" style="border: 1px solid #000000;" />
    </div>
    <div class="col-md-1">
      Edad:
    </div>
    <div class="col-md-2">
      <hr align="left" style="border: 1px solid #000000;" />
    </div>
    <div class="col-md-2">
      <hr align="left" style="border: 1px solid #000000;" />
    </div>
  </div>
  <div class="form-group row" style="border: 1px solid #000000;">
    <div class="col-md-2">
      Fecha y lugar de nacimiento:
    </div>
    <div class="col-md-4">
      <hr align="left" style="border: 1px solid #000000;" />
    </div>
    <div class="col-md-2">
      Tipo de sangre:
    </div>
    <div class="col-md-2">
      <hr align="left" style="border: 1px solid #000000;" />
    </div>
    <div class="col-md-2">
      <hr align="left" style="border: 1px solid #000000;" />
    </div>
  </div>
  <div class="form-group row" style="border: 1px solid #000000;">
    <div class="col-md-2">
      Fecha de ingreso:
    </div>
    <div class="col-md-4">
      <hr align="left" style="border: 1px solid #000000;" />
    </div>
    <div class="col-md-2 col-md-offset-4">
      <hr align="left" style="border: 1px solid #000000;" />
    </div>
  </div>
  -->

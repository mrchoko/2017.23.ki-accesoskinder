    <form 
        id="form-asigna-accion" name="form-asigna-accion" 
        method="post" 
        action="./index.php?seccion=grupo&accion=asigna_accion&grupo_id=<?php echo $grupo_id; ?>">
        <?php echo $directiva->div_titulo($grupo[0]['descripcion']); ?>
        <div class='row well'>
            <div class='col-md-12 scroll'>
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default" id="chk-asigna-accion">
                        <?php
                        if (!empty($secciones)) {
                            foreach ($secciones as $key => $seccion) {
                                echo $directiva->div_subtitulo($seccion['id'], $seccion['descripcion']); 
                                echo "<div id='".$seccion['id']."' class='panel-collapse collapse in'>";
                                echo "<div class='panel-body'>";
                                echo "<ul class='list-group' id='lista-seccion-accion'>";
                                if(isset ($acciones)){
                                    foreach ($acciones as $key => $accion) {
                                        if($seccion['descripcion'] == $accion['descripcion_seccion']){
echo "<li class='list-group-item'>".$accion['descripcion'];
    echo "<div class='material-switch pull-right'>";
        echo $directiva->input_checkbox_seccion($acciones_grupos, $grupo, $accion, $seccion);
    echo "<label for='".$accion['descripcion']."' class='label-primary'></label>";
        echo "</div>";
                                                echo "</li>";
                                        }   
                                }
                                }
                                echo "</ul>";
                                echo "</div>";
                                echo "</div>";
                            }
                        }
                        ?>
                    </div>
                </div>
            </div> 
        </div>
        <div class="form-group text-center">
            <input type='hidden' id='grupo_id' value='<?php echo $grupo[0]['id']; ?>'>
        </div>
    </form>
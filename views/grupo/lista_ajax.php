<?php 
  $contador = 0;
  foreach ($grupos as $key => $grupo) {
    $salto_linea = $contador%3;
    $contador++;
    if($salto_linea == 0){
      echo "<div class='row'>";
    }
  $panel_class = "";
  if($grupo['status'] == 1){
		$panel_class = 'panel panel-info';
	}
	else{
  	$panel_class = 'panel panel-danger';	
	}
?>
<div class="col-md-4 registro">
  <div class="<?php echo $panel_class; ?>">
  	<?php echo $directiva->div_encabezado_registro_lista($grupo['id'], $seccion); ?>
  	<div class="panel-body">
  	<?php 
  	 echo $directiva->span_registro_general($grupo['descripcion'], $grupo['status']);
  	?>
  	<br>
  	<?php
			echo $directiva->link_elimina($seccion, $grupo['id']);
			echo $directiva->link_cambia_status($seccion, $grupo['id'], $grupo['status']); 
			echo $directiva->link_modifica($seccion, $grupo['id']);
   		echo $directiva->link_asigna_accion($seccion, $grupo['id']);
   	?>
    </div>
  </div>
</div>
<?php
  if($salto_linea == 2){
    echo "</div>";
  }
}
if($salto_linea < 2){
  echo "</div>";
}
?>

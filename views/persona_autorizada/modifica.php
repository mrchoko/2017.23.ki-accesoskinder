	<form 
		id="form-persona-autorizada-modifica" name="form-persona-autorizada-modifica" 
		method="post" 
		action="./index.php?seccion=persona_autorizada&accion=modifica_bd&persona_autorizada_id=<?php echo $persona_autorizada_id; ?>" enctype="multipart/form-data">

		<div class="form-group row">
			<div class="col-md-6">
				<input 
					type="text" class="form-control" 
					name="nombre" placeholder="Ingresa Nombre" 
					required title="Ingrese un Nombre"
					value='<?php echo $persona_autorizada[0]['nombre']; ?>'>
			</div>
			<div class="col-md-6">
				<input 
					type="text" class="form-control" 
					name="apellido_paterno" placeholder="Ingresa Apellido Paterno" 
					required title="Ingrese Apellido Paterno"
					value='<?php echo $persona_autorizada[0]['apellido_paterno']; ?>'>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-6">
				<input 
					type="text" class="form-control" 
					name="apellido_materno" placeholder="Ingresa Apellido Materno" 
					required title="Ingrese Apellido Materno"
					value='<?php echo $persona_autorizada[0]['apellido_materno']; ?>'>
			</div>
			<div class="col-md-6">
                <div class='input-group date' id='divMiCalendario'>
                      <input type='text' id="fecha_nacimiento" name="fecha_nacimiento" class="form-control"  readonly value='<?php echo $persona_autorizada[0]['fecha_nacimiento']; ?>'/>                      
                      <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                      </span>
                </div>
	        </div>
	    </div>
	    <div class="form-group row">
			<div class="col-md-12">
            	<input 
					type="text" class="form-control" 
					name="domicilio" placeholder="Ingresa Domicilio" 
					required title="Ingrese Domicilio" value='<?php echo $persona_autorizada[0]['domicilio']; ?>'>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-3">
            	<input type="text" class="form-control input-medium bfh-phone" 
            	data-format="ddd-ddd-dddd" maxlength="12" placeholder="Ingrese Telefono"
            	name="telefono" value='<?php echo $persona_autorizada[0]['telefono']; ?>'>
			</div>
			<div class="col-md-3 text-center">
				<a href="index.php?seccion=persona_autorizada&accion=modifica&codigo=1&persona_autorizada_id=<?php echo $_GET['persona_autorizada_id']; ?>&nombre_codigo=<?php echo $persona_autorizada[0]['foto']; ?>">
  					<button type="button" class="btn btn-secondary">Cambiar Código de Barras</button>
				</a>
            </div>

			<div class="col-md-3 text-right">
				<input type="file" accept="image/png, image/jpeg" name="foto"/>
            </div>

            <div class="col-md-3 text-center">
            	<img class="foto" src="<?php echo './views/persona_autorizada/fotos/'.$persona_autorizada[0]['foto']; ?>">
            </div>


		</div>
		<div class="form-group text-center row">
			<div class="col-md-12">
				<textarea class="form-control noresize" name="observaciones" rows="3" placeholder="Ingresa Observaciones" title="Ingrese una observacion" value='<?php echo $persona_autorizada[0]['observaciones']; ?>'></textarea>
			</div>
		</div>
		<div class="form-group text-center row">
			<div class="col-md-12">
				<button type="submit" class="btn btn-secondary">Enviar</button>
				<input type="hidden" name="foto_anterior" value='<?php echo $persona_autorizada[0]['foto']; ?>'>
				<input type='hidden' name='status' value='<?php echo $persona_autorizada[0]['status']; ?>'>
				<input type='hidden' name='codigo_barras' value='<?php echo $persona_autorizada[0]['codigo_barras']; ?>'>
			</div>
		</div>
	</form>
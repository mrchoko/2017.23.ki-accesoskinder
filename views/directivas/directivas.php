<?php
class Directivas{

	public function input_checkbox_seccion($acciones_grupos, $grupo, $accion, $seccion){
		$html = "";
        if(!empty($acciones_grupos)){
            $asignacion_valores = $this->obten_valor_checked(
                $acciones_grupos, $grupo, $accion, $seccion);
            $checked = $asignacion_valores[1];
            $valor = $asignacion_valores[0];
            $html = $html.$this->input_checkbox($accion['descripcion'], $accion['id'], $valor, $checked);
        }
        return $html;
	}
	public function obten_valor_checked($acciones_grupos, $grupo, $accion, $seccion){
    	$checked = "";
       	$valor = "";
  		foreach ($acciones_grupos as $key => $valores) {
      		if(
      			$grupo[0]['descripcion'] == $valores['descripcion_grupo'] && 
      			$accion['descripcion'] == $valores['descripcion'] && 
      			$seccion['descripcion'] == $accion['descripcion_seccion']){

       			$valor = $valores['id'];
            	$checked = "checked";
           		return array($valor, $checked);
        	}
      	}  
	}
	public function input_checkbox($id,$name,$valor,$checked){
            $html = "<input id='".$id."' name='".$name."' type='checkbox' value = '$valor' $checked />";
            return $html;
	}

	public function div_subtitulo($link, $descripcion){
		$html = "
		<div class='panel-heading'>
			<h4 class='panel-title'>
				<a data-toggle='collapse' data-parent='#accordion' href='#$link'>
					$descripcion
				</a>
			</h4>
     	</div>";
     	return $html;		
	}
	public function div_titulo($titulo){
		$html = "
        <div class='text-center'>
            <h3>
            $titulo
            </h3>
        </div>";
        return $html;
	}
	public function span_lista_descripcion($contenido){
		$html = "<span><label>Descripcion:</label>$contenido</span>";
		return $html;
	}
	public function span_lista_status($status){
		$html = "<span><label>Status:</label>"; 
  		if($status==1){ 
  			$html = $html.'Activo';
  		}
		else{ 
			$html = $html.'Inactivo'; 
		}
		$html = $html."</span>";
		return $html;
	}
	public function div_encabezado_registro_lista($contenido, $seccion){
		$seccion =  ucfirst ( $seccion);
		$html = "<div class='panel-heading'>$seccion: $contenido</div>";
		return $html;
	}
	public function link_elimina($seccion, $id){
		$html = "
		<a href='index.php?seccion=".$seccion."&accion=elimina&".$seccion."_id=".$id."' class='link_accion'>
			<span class='glyphicon glyphicon-trash' aria-hidden='true'></span>
  		</a>";
  		return $html;
	}
	public function link_desactiva($seccion, $id){
		$html = "
		<a href='index.php?seccion=".$seccion."&accion=desactiva&".$seccion."_id=$id' class='link_accion'>
  			<span class='glyphicon glyphicon-minus' aria-hidden='true'></span>
  		</a>";
  		return $html;
	}
	public function link_activa($seccion, $id){
		$html = "<a href='index.php?seccion=$seccion&accion=activa&".$seccion."_id=$id' class='link_accion'>
  					<span class='glyphicon glyphicon-ok' aria-hidden='true'></span>
  				</a>";
  		return $html;
	}
	public function link_modifica($seccion,$id){
		$html = "<a href='index.php?seccion=$seccion&accion=modifica&".$seccion."_id=$id'>
  					<span class='glyphicon glyphicon-pencil' aria-hidden='true'></span>
  				</a>";
  		return $html;
	}

	public function link_asigna_accion($seccion,$id){
		$html = "<a href='index.php?seccion=$seccion&accion=asigna_accion&".$seccion."_id=$id'>
  					<span class='glyphicon glyphicon-lock' aria-hidden='true'></span>
  				</a>";
  		return $html;		
	}

	public function link_cambia_status($seccion, $id, $status){
		$html = "";
		if($status==1){
  			$html = $html.$this->link_desactiva($seccion, $id); 
  		}
  		else{ 
  			$html = $html.$this->link_activa($seccion, $id);
  		} 
  		return $html;		
	}

	public function div_busqueda(){
		if (isset($_GET['valor'])){
			$valor_filtro = $_GET['valor'];
		}
		else{
			$valor_filtro = "";
		}
		$html = "
			<div class='input-group col-md-6 col-md-offset-6 busqueda'>
				<span class='input-group-addon'>
					<span class='glyphicon glyphicon-search' aria-hidden='true'></span>
				</span>
				<input  type='text' class='form-control input-md busca_registros' placeholder='Ingresa Busqueda' value='$valor_filtro'>
			</div>";
		return $html;
	}
	public function span_registro_general($descripcion, $status){
		$html = "";
		$html = $html.$this->span_lista_descripcion($descripcion);
		$html = $html." ";
  		$html = $html.$this->span_lista_status($status);
  		return $html;
	}
}
?>
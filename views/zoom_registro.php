<div id="zoom_registro" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg modal_registro">
	    <div class="modal-content">
	        <div class="modal-body text-center">
	        	<div class="row">
	        		<div class="col-xs-4" id="foto_registro">
		            </div>

		            <div class="col-xs-8">

					<div id="carousel_registro" class="carousel slide" data-ride="carousel">
						<!-- Wrapper for slides -->
						<div class="carousel-inner" id="fotos">
						</div>

						<!-- Controls -->
						<!--
						<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
						<span class="glyphicon glyphicon-chevron-left"><</span>
						</a>
						<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
						<span class="glyphicon glyphicon-chevron-right">></span>
						</a> -->
					</div>

					</div>

				</div>

		    </div>
	    </div>
	</div>
</div>
<?php
if (!empty($tokens)) {
?>
	<div class="container">
		<div class="row hiden-btn">
			<div class="input-group col-xs-6 col-xs-offset-6">
				<span class="input-group-addon">Busqueda</span>
					<input id="busqueda-1" type="text" class="form-control" placeholder="Ingresa Busqueda">
			</div>
		</div>
		<div class="row hidden">
			<div class="input-group col-xs-12">
				<span class="input-group-addon">Busqueda</span>
					<input id="busqueda-2" type="text" class="form-control" placeholder="Ingresa Busqueda">
			</div>
		</div>
	</div>
	<div class="row">&nbsp;</div>
	<div class="container">
		<div class="row">
			<div class="panel panel-default" id="lista_grupo">
					<table class="table table-fixed">
    					<thead>
      						<tr>
      							<th class="col-xs-1">Id</th>
        						<th class="col-xs-3">Usuario</th>
        						<th class="col-xs-5">Token</th>
        						<th class="col-xs-3 text-center">Acciones</th>
      						</tr>
    					</thead>
    					<tbody class="registros">
						<?php foreach ($tokens as $key => $token) { ?>
							<tr>
								<td class="col-xs-1"><?php echo $token['id']; ?></td>
								<td class="col-xs-3"><?php echo $token['nombre_usuario']; ?></td>
								<td class="col-xs-5 ocultar-texto"><?php echo $token['token']; ?></td>
								<td class="col-xs-3 text-center hiden-btn">
									<div class="btn-group">
										<a href="index.php?seccion=token&accion=elimina&token_id=<?php echo $token['id']; ?>">
  											<button type="button" class="btn btn-danger" >
  												Elimina
  											</button>
  										</a>
  									</div>
  								</td>


  								<td class="col-xs-1 hidden"></td>
								<td class="col-xs-2 text-center hidden">

								    <div class="btn-group">
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
									    	<span>Acciones</span>
									    	<span></span>
									    	<span class="caret"></span>
										</button>
										<ul class="dropdown-menu dropdown-menu-right" role="menu">
											<li>
												<a href="index.php?seccion=token&accion=elimina&token_id=<?php echo $token['id']; ?>">
		  											<button type="button" class="btn btn-secondary btn-outline" >
		  												Elimina
		  											</button>
		  										</a>
											</li>
										</ul>
									</div>

								</td>

							</tr>
						<?php } ?>
						</tbody>
					</table>
			</div>
		</div>
	</div>
<?php
}
?>
	
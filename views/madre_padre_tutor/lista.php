<?php
if (!empty($madre_padre_tutor)) {
?>
	<div class="container">
		<div class="row hiden-btn">
			<div class="col-xs-3">
				<div class="btn-group">
					<a href="index.php?seccion=credencial&accion=credencial_general&tabla=madre_padre_tutor">
  					<button type="button" class="btn btn-secondary">
  						Generar Credenciales
  					</button>
  					</a>  										
				</div>
			</div>
			<div class="input-group col-xs-6 col-xs-offset-6">
				<span class="input-group-addon">Busqueda</span>
					<input id="busqueda-1" type="text" class="form-control" placeholder="Ingresa Busqueda">
			</div>
		</div>
		<div class="row hidden">
			<div class="input-group col-xs-12">
				<span class="input-group-addon">Busqueda</span>
					<input id="busqueda-2" type="text" class="form-control" placeholder="Ingresa Busqueda">
			</div>
		</div>
	</div>
	<div class="row">&nbsp;</div>
	<div class="container">
		<div class="row">
			<div class="panel panel-default" id="lista_madre_padre_tutor">
					<table class="table table-fixed">
    					<thead>
      						<tr>
      							<th class="col-xs-1">Id</th>
        						<th class="col-xs-3">Nombre</th>
        						<th class="col-xs-1">Estatus</th>
        						<th class="col-xs-2 text-center">Foto</th>
        						<th class="col-xs-5 text-center">Acciones</th>
      						</tr>
    					</thead>
    					<tbody class="registros">
						<?php foreach ($madre_padre_tutor as $key => $registros) { ?>
							<tr>
								<td class="col-xs-1"><?php echo $registros['id']; ?></td>
								<td class="col-xs-3 ocultar-texto"><?php echo $registros['nombre'].' '.$registros['apellido_paterno'].' '.$registros['apellido_materno']; ?></td>
								<td class="col-xs-1">
									<div>
										<?php if($registros['status']==1){ echo 'Activo';}
												else{ echo 'Inactivo'; } ?>
									</div>
								</td>
								<td class="col-xs-2 text-center">
									<img data-toggle="modal" data-target="#zoom_foto" class="foto" src="<?php echo './views/madre_padre_tutor/fotos/'.$registros['foto']; ?>">
								</td>
								<td class="col-xs-1 text-center hiden-btn">
									<div class="btn-group">
										<a href="index.php?seccion=madre_padre_tutor&accion=elimina&madre_padre_tutor_id=<?php echo $registros['id']; ?>&foto=<?php echo $registros['foto']; ?>&codigo_barras=<?php echo $registros['nombre'].''.$registros['apellido_paterno'].''.$registros['apellido_materno'].'.jpg'; ?>">
  											<button type="button" class="btn btn-danger" >
  												Elimina
  											</button>
  										</a>
  									</div>
  								</td>
  								<td class="col-xs-2 text-center hiden-btn">
									<div class="btn-group">
  										<?php if($registros['status']==1){ ?>
  										<a href="index.php?seccion=madre_padre_tutor&accion=desactiva&madre_padre_tutor_id=<?php echo $registros['id']; ?>">
  											<button type="button" class="btn btn-primary" >
  												Desactiva
  											</button>
  										</a>
  										<?php }
  										else{ ?>

  										<a href="index.php?seccion=madre_padre_tutor&accion=activa&madre_padre_tutor_id=<?php echo $registros['id']; ?>">
  											<button type="button" class="btn btn-primary">
  												Activa
  											</button>
  										</a>
  										<?php
  										}
  										?>
  									</div>
  								</td>
  								<td class="col-xs-1 text-center hiden-btn">
									<div class="btn-group">
  										<a href="index.php?seccion=madre_padre_tutor&accion=modifica&madre_padre_tutor_id=<?php echo $registros['id']; ?>">
  										<button type="button" class="btn btn-primary">
  											Modifica
  										</button>
  										</a>
									</div>
								</td>
								<td class="col-xs-1 text-center hiden-btn">
									<div class="btn-group">
										<a href="index.php?seccion=credencial&accion=credencial_individual&nombre=<?php echo $registros['nombre'].' '.$registros['apellido_paterno'].' '.$registros['apellido_materno']; ?>&foto=<?php echo $registros['foto']; ?>&credencial=<?php echo pathinfo($registros['foto'],PATHINFO_FILENAME); ?>&tabla=madre_padre_tutor">
  										<button type="button" class="btn btn-primary">
  											Credencial
  										</button>
  										</a>  										
									</div>
								</td>

								<td class="col-xs-1 hidden"></td>
								<td class="col-xs-4 text-center hidden">

								    <div class="btn-group">
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
									    	<span>Acciones</span>
									    	<span></span>
									    	<span class="caret"></span>
										</button>
										<ul class="dropdown-menu dropdown-menu-right" role="menu">
											<li>
												<a href="index.php?seccion=madre_padre_tutor&accion=elimina&madre_padre_tutor_id=<?php echo $registros['id']; ?>&foto=<?php echo $registros['foto']; ?>&codigo_barras=<?php echo $registros['nombre'].''.$registros['apellido_paterno'].''.$registros['apellido_materno'].'.jpg'; ?>">
		  											<button type="button" class="btn btn-secondary btn-outline" >
		  												Elimina
		  											</button>
		  										</a>
											</li>
											<li>
												<?php if($registros['status']==1){ ?>
		  										<a href="index.php?seccion=madre_padre_tutor&accion=desactiva&madre_padre_tutor_id=<?php echo $registros['id']; ?>">
		  											<button type="button" class="btn btn-secondary btn-outline" >
		  												Desactiva
		  											</button>
		  										</a>
		  										<?php }
		  										else{ ?>

		  										<a href="index.php?seccion=madre_padre_tutor&accion=activa&madre_padre_tutor_id=<?php echo $registros['id']; ?>">
		  											<button type="button" class="btn btn-secondary btn-outline">
		  												Activa
		  											</button>
		  										</a>
		  										<?php
		  										}
		  										?>
											</li>
											<li>
												<a href="index.php?seccion=madre_padre_tutor&accion=modifica&madre_padre_tutor_id=<?php echo $registros['id']; ?>">
		  										<button type="button" class="btn btn-secondary btn-outline">
		  											Modifica
		  										</button>
		  										</a>
											</li>
										</ul>
									</div>

								</td>


							</tr>
						<?php } ?>
						</tbody>
					</table>
			</div>
		</div>
	</div>
<?php
}
?>
	
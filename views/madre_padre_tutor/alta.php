	<form 
		id="form-madre_padre_tutor-alta" name="form-madre_padre_tutor-alta" 
		method="post" 
		action="./index.php?seccion=madre_padre_tutor&accion=alta_bd" enctype="multipart/form-data">

		<div class="form-group row">
			<div class="col-md-6">
				<input 
					type="text" class="form-control" 
					name="nombre" placeholder="Ingresa Nombre" 
					required title="Ingrese un Nombre">
			</div>
			<div class="col-md-6">
				<input 
					type="text" class="form-control" 
					name="apellido_paterno" placeholder="Ingresa Apellido Paterno" 
					required title="Ingrese Apellido Paterno">
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-6">
				<input 
					type="text" class="form-control" 
					name="apellido_materno" placeholder="Ingresa Apellido Materno" 
					required title="Ingrese Apellido Materno">
			</div>
			<div class="col-md-6">
                <div class='input-group date' id='divMiCalendario'>
                      <input type='text' id="fecha_nacimiento" name="fecha_nacimiento" class="form-control"  readonly/>                      
                      <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                      </span>
                </div>
	        </div>
	    </div>
	    <div class="form-group row">
			<div class="col-md-12">
            	<input 
					type="text" class="form-control" 
					name="domicilio" placeholder="Ingresa Domicilio" 
					required title="Ingrese Domicilio">
			</div>
		</div>
		<div class="form-group row">

			<div class="col-md-3">
            	<input type="text" class="form-control input-medium bfh-phone" 
            	data-format="ddd-ddd-dddd" maxlength="12" placeholder="Ingrese Telefono"
            	name="telefono">
			</div>

			<div class="col-md-3 text-right">
		    	<label class="switch1">
					<input type="checkbox" name="status" >
					<div class="slider1"></div>
				</label>
			</div>

			<div class="col-md-6 text-right">
				<input type="file" accept="image/png, image/jpeg" name="foto"/>
            </div>

		</div>
		<div class="form-group text-center row">
			<div class="col-md-12">
				<textarea class="form-control noresize" name="observaciones" rows="3" placeholder="Ingresa Observaciones" title="Ingrese una observacion"></textarea>
			</div>
		</div>
		<div class="form-group text-center row">
			<div class="col-md-12">
				<button type="submit" class="btn btn-secondary">Enviar</button>
			</div>
		</div>
	</form>
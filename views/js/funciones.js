$(document).ready(function () {
    $('.busca_registros').change(function (){
        var valor_consulta = $(this).val();
        $.ajax({ 
            url: "./index_ajax.php?seccion=grupo&accion=lista_ajax",
            type: "POST", //send it through get method
            data: {valor: valor_consulta},
            success: function(data) {
                $('#contenido_lista').empty();
                $('#contenido_lista').append(data);
            },
            error: function(xhr, status) {
                //Do Something to handle error
                //alert("no insertado correctamente");
            }
        });
    });

    $('.link_accion').click(function (){
        var valor = $('.busca_registros').val();
        var link_accion = $(this).attr("href");
        var valor = '&valor='+valor;
        var ruta = link_accion+valor
        $(this).attr({'href':ruta});
        var link_accion = $(this).attr("href");  
    });

    //busqueda-madre-padre-tutor
    $('#busqueda-madre-padre-tutor').keyup(function () {
        var rex = new RegExp($(this).val(), 'i');
        $('.registros-madre-padre-tutor tr').hide();
        $('.registros-madre-padre-tutor tr').filter(function () {
            return rex.test($(this).text());
        }).show();

        });
    $('#busqueda-persona-autorizada').keyup(function () {
        var rex = new RegExp($(this).val(), 'i');
        $('.registros-persona-autorizada tr').hide();
        $('.registros-persona-autorizada tr').filter(function () {
            return rex.test($(this).text());
        }).show();

        });
    $('.foto').click(function() {
        var src =$(this).attr('src');
        $('.zoom_img').attr('src', src);
    });

    $("#chk-madre-padre-tutor :checkbox").click(function(){
            var madre_padre_tutor = $(this).attr('name');
            var alumno = $( "#alumno_id" ).val();
            var asigna_madre_padre_tutor = $(this).attr('value');
            //alert(madre_padre_tutor + '-' + alumno + '-' + asigna_madre_padre_tutor);

            if ($(this).prop("checked") == true){

                $.ajax({ //./index.php?seccion=accion_grupo&accion=alta_db
                        url: "./index.php?seccion=asigna_madre_padre_tutor&accion=alta_bd",
                        type: "POST", //send it through get method
                        data: {
                            alumno_id: alumno,
                            madre_padre_tutor_id: madre_padre_tutor                         
                        },
                        success: function(data) {
                            //Do Something
                            //alert("insertado correctamente");
                        },
                        error: function(xhr, status) {
                            //Do Something to handle error
                            //alert("no insertado correctamente");
                        }
                    });
                location.reload();
                $(this).attr("checked") = "checked";

            } else {
                
                $.ajax({ //index.php?seccion=accion_grupo&accion=elimina&accion_grupo_id; 
                        url: "./index.php?seccion=asigna_madre_padre_tutor&accion=elimina&asigna_madre_padre_tutor_id="+asigna_madre_padre_tutor,
                        type: "POST", //send it through get method
                        data: {
                            asigna_madre_padre_tutor_id: asigna_madre_padre_tutor
                        },
                        success: function(data) {
                            //Do Something
                            //alert("eliminado correctamente");
                        },
                        error: function(xhr, status) {
                            //Do Something to handle error
                            //alert("no eliminado correctamente");
                        }
                    });
                location.reload();
                $(this).attr("checked") = "";
            }

            return false;
    });

    $("#chk-persona-autorizada :checkbox").click(function(){
            var persona_autorizada = $(this).attr('name');
            var alumno = $( "#alumno_id" ).val();
            var asigna_persona_autorizada = $(this).attr('value');
            //alert(madre_padre_tutor + '-' + alumno + '-' + asigna_madre_padre_tutor);

            if ($(this).prop("checked") == true){

                $.ajax({ //./index.php?seccion=accion_grupo&accion=alta_db
                        url: "./index.php?seccion=asigna_persona_autorizada&accion=alta_bd",
                        type: "POST", //send it through get method
                        data: {
                            alumno_id: alumno,
                            persona_autorizada_id: persona_autorizada                           
                        },
                        success: function(data) {
                            //Do Something
                            //alert("insertado correctamente");
                        },
                        error: function(xhr, status) {
                            //Do Something to handle error
                            //alert("no insertado correctamente");
                        }
                    });
                location.reload();
                $(this).attr("checked") = "checked";

            } else {
                
                $.ajax({ //index.php?seccion=accion_grupo&accion=elimina&accion_grupo_id; 
                        url: "./index.php?seccion=asigna_persona_autorizada&accion=elimina&asigna_persona_autorizada_id="+asigna_persona_autorizada,
                        type: "POST", //send it through get method
                        data: {
                            asigna_persona_autorizada_id: asigna_persona_autorizada
                        },
                        success: function(data) {
                            //Do Something
                            //alert("eliminado correctamente");
                        },
                        error: function(xhr, status) {
                            //Do Something to handle error
                            //alert("no eliminado correctamente");
                        }
                    });
                location.reload();
                $(this).attr("checked") = "";
            }

            return false;
    });

    $("#chk-asigna-accion :checkbox").click(function(){
            var accion = $(this).attr('name');
            var grupo = $( "#grupo_id" ).val();
            var accion_grupo = $(this).attr('value');
            if ($(this).prop("checked") == true){
                $.ajax({ //./index.php?seccion=accion_grupo&accion=alta_db
                        url: "./index.php?seccion=accion_grupo&accion=alta_bd",
                        type: "POST", //send it through get method
                        data: {
                            grupo_id: grupo,
                            accion_id: accion
                        },
                        success: function(data) {
                            //Do Something
                            //alert("insertado correctamente");
                        },
                        error: function(xhr, status) {
                            //Do Something to handle error
                            //alert("no insertado correctamente");
                        }
                    });
                location.reload();
                $(this).attr("checked") = "checked";

            } else {
                
                $.ajax({ //index.php?seccion=accion_grupo&accion=elimina&accion_grupo_id; 
                        url: "./index.php?seccion=accion_grupo&accion=elimina&accion_grupo_id="+accion_grupo,
                        type: "POST", //send it through get method
                        data: {
                            accion_grupo_id: accion_grupo
                        },
                        success: function(data) {
                            //Do Something
                            //alert("eliminado correctamente");
                        },
                        error: function(xhr, status) {
                            //Do Something to handle error
                            //alert("no eliminado correctamente");
                        }
                    });
                location.reload();
                $(this).attr("checked") = "";
            }
            return false;
    });
});
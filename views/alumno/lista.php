<?php
if (!empty($alumno)) {
?>
	<div class="container">
		<div class="row hiden-btn">
			<div class="col-xs-3">
				<div class="btn-group">
					<a href="index.php?seccion=credencial&accion=credencial_general&tabla=alumno">
  					<button type="button" class="btn btn-secondary black">
  						Generar Credenciales
  					</button>
  					</a>  										
				</div>
			</div>
			<div class="input-group col-xs-6 col-xs-offset-6">
				<span class="input-group-addon">Busqueda</span>
					<input id="busqueda-1" type="text" class="form-control" placeholder="Ingresa Busqueda">
			</div>
		</div>
		<div class="row hidden">
			<div class="input-group col-xs-12">
				<span class="input-group-addon">Busqueda</span>
					<input id="busqueda-2" type="text" class="form-control" placeholder="Ingresa Busqueda">
			</div>
		</div>
	</div>
	<div class="row">&nbsp;</div>
	<div class="container">
		<div class="row">
			<div class="panel panel-default" id="lista_alumno">
					<table class="table table-fixed">
    					<thead>
      						<tr>
      							<th class="col-xs-1">Id</th>
        						<th class="col-xs-3">Nombre</th>
        						<th class="col-xs-1">Estatus</th>
        						<th class="col-xs-2 text-center">Foto</th>
        						<th class="col-xs-5 text-center">Acciones</th>
      						</tr>
    					</thead>
    					<tbody class="registros">
						<?php foreach ($alumno as $key => $registros) { ?>
							<tr>
								<td class="col-xs-1"><?php echo $registros['id']; ?></td>
								<td class="col-xs-3 ocultar-texto">
									<a class="black" title="Asignar Madre, Padre, Tutor y/o Persona Autorizada" href="index.php?seccion=alumno&accion=asigna_mpt_pa&alumno_id=<?php echo $registros['id']; ?>">
									<?php echo $registros['nombre'].' '.$registros['apellido_paterno'].' '.$registros['apellido_materno']; ?>
									</a>
								</td>
								<td class="col-xs-1">
									<div>
										<?php if($registros['status']==1){ echo 'Activo';}
												else{ echo 'Inactivo'; } ?>
									</div>
								</td>
								<td class="col-xs-2 text-center">
									<img data-toggle="modal" data-target="#zoom_foto" class="foto" src="<?php echo './views/alumno/fotos/'.$registros['foto']; ?>">
								</td>
								<td class="col-xs-1 text-center hiden-btn">
									<div class="btn-group">
										<a href="index.php?seccion=alumno&accion=elimina&alumno_id=<?php echo $registros['id']; ?>&foto=<?php echo $registros['foto']; ?>&codigo_barras=<?php echo $registros['nombre'].''.$registros['apellido_paterno'].''.$registros['apellido_materno'].'.jpg'; ?>">
  											<button type="button" class="btn btn-danger" >
  												Elimina
  											</button>
  										</a>
  									</div>
  								</td>
  								<td class="col-xs-2 text-center hiden-btn">
									<div class="btn-group">
  										<?php if($registros['status']==1){ ?>
  										<a href="index.php?seccion=alumno&accion=desactiva&alumno_id=<?php echo $registros['id']; ?>">
  											<button type="button" class="btn btn-primary" >
  												Desactiva
  											</button>
  										</a>
  										<?php }
  										else{ ?>

  										<a href="index.php?seccion=alumno&accion=activa&alumno_id=<?php echo $registros['id']; ?>">
  											<button type="button" class="btn btn-primary">
  												Activa
  											</button>
  										</a>
  										<?php
  										}
  										?>
  									</div>
  								</td>

  								<td class="col-xs-1 text-center hiden-btn">
									<div class="btn-group">
  										<a href="index.php?seccion=alumno&accion=modifica&alumno_id=<?php echo $registros['id']; ?>">
  										<button type="button" class="btn btn-primary">
  											Modifica
  										</button>
  										</a>
									</div>
								</td>
								<td class="col-xs-1 text-center hiden-btn">
									<div class="btn-group">
										<a href="index.php?seccion=credencial&accion=credencial_individual&nombre=<?php echo $registros['nombre'].' '.$registros['apellido_paterno'].' '.$registros['apellido_materno']; ?>&foto=<?php echo $registros['foto']; ?>&credencial=<?php echo pathinfo($registros['foto'],PATHINFO_FILENAME); ?>&tabla=alumno">
  										<button type="button" class="btn btn-primary">
  											Credencial
  										</button>
  										</a>  										
									</div>
								</td>

								<td class="col-xs-1 hidden"></td>
								<td class="col-xs-4 text-center hidden">

								    <div class="btn-group">
										<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
									    	<span>Acciones</span>
									    	<span></span>
									    	<span class="caret"></span>
										</button>
										<ul class="dropdown-menu dropdown-menu-right" role="menu">
											<li>
												<a href="index.php?seccion=alumno&accion=elimina&alumno_id=<?php echo $registros['id']; ?>&foto=<?php echo $registros['foto']; ?>&codigo_barras=<?php echo $registros['nombre'].''.$registros['apellido_paterno'].''.$registros['apellido_materno'].'.jpg'; ?>">
		  											<button type="button" class="btn btn-secondary btn-outline" >
		  												Elimina
		  											</button>
		  										</a>
											</li>
											<li>
												<?php if($registros['status']==1){ ?>
		  										<a href="index.php?seccion=alumno&accion=desactiva&alumno_id=<?php echo $registros['id']; ?>">
		  											<button type="button" class="btn btn-secondary btn-outline" >
		  												Desactiva
		  											</button>
		  										</a>
		  										<?php }
		  										else{ ?>

		  										<a href="index.php?seccion=alumno&accion=activa&alumno_id=<?php echo $registros['id']; ?>">
		  											<button type="button" class="btn btn-secondary btn-outline">
		  												Activa
		  											</button>
		  										</a>
		  										<?php
		  										}
		  										?>
											</li>
											<li>
												<a href="index.php?seccion=alumno&accion=modifica&alumno_id=<?php echo $registros['id']; ?>">
		  										<button type="button" class="btn btn-secondary btn-outline">
		  											Modifica
		  										</button>
		  										</a>
											</li>
										</ul>
									</div>

								</td>


							</tr>
						<?php } ?>
						</tbody>
					</table>
			</div>
		</div>
	</div>

<?php
}
?>
	
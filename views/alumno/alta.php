	<form 
		id="form-alumno-alta" name="form-alumno-alta" 
		method="post" 
		action="./index.php?seccion=alumno&accion=alta_bd" enctype="multipart/form-data">

		<div class="form-group row">
			<div class="col-md-6">
				<input 
					type="text" class="form-control" 
					name="nombre" placeholder="Ingresa Nombre" 
					required title="Ingrese un Nombre">
			</div>
			<div class="col-md-6">
				<input 
					type="text" class="form-control" 
					name="apellido_paterno" placeholder="Ingresa Apellido Paterno" 
					required title="Ingrese Apellido Paterno">
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-6">
				<input 
					type="text" class="form-control" 
					name="apellido_materno" placeholder="Ingresa Apellido Materno" 
					required title="Ingrese Apellido Materno">
			</div>
			<div class="col-md-6">
                <div class='input-group date' id='calendario_fecha_nacimiento'>
                    <input type='text' id="fecha_nacimiento" name="fecha_nacimiento" class="form-control" placeholder="Ingresa Fecha De Nacimiento" readonly/>
                    <span class="input-group-addon">
                    	<span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
	        </div>
	    </div>
	    <div class="form-group row">
	    	<div class="col-md-6">
				<input class="form-control noresize" name="lugar_nacimiento" placeholder="Ingresa Lugar De Nacimiento" title="Ingrese un lugar de nacimiento" required/>
			</div>
			<div class="col-md-6 text-right">
				<input type="file" accept="image/png, image/jpeg" name="foto" rows="2"/>
            </div>
		</div>
		<div class="form-group row">
			<div class="col-md-6">
				<select name="genero" class="selectpicker" data-live-search="true" 
				title="Seleccione Genero" data-width="100%" 
				data-none-results-text="No se encontraron resultados" required>
			        	<option value="hombre">Hombre</option>
			        	<option value="mujer">Mujer</option>
			    </select>
			</div>
			<div class="col-md-6">
				<select name="tipo_sangre" class="selectpicker" data-live-search="true" 
				title="Seleccione Tipo De Sangre" data-width="100%" 
				data-none-results-text="No se encontraron resultados" required>
			        	<option value="A Positiva">A Positiva</option>
			        	<option value="A Negativo">A Negativo</option>
			        	<option value="B Positivo">B Positivo</option>
			        	<option value="B Negativo">B Negativo</option>
			        	<option value="O Positivo">O Positivo</option>
			        	<option value="O Negativo">O Negativo</option>
			        	<option value="AB Positivo">AB Positivo</option>
			        	<option value="AB Negativo">AB Negativo</option>
			    </select>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-6">
                <div class='input-group date' id='calendario_fecha_ingreso'>
                    <input type='text' id="fecha_ingreso" name="fecha_ingreso" class="form-control" placeholder="Ingresa Fecha De Ingreso" readonly/>
                    <span class="input-group-addon">
                    	<span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
	        </div>
			<div class="col-md-6 text-right">
		    	<label class="switch1">
					<input type="checkbox" name="status" >
					<div class="slider1"></div>
				</label>
			</div>
		</div>
		<div class="form-group text-center row">
			<div class="col-md-12">
				<textarea class="form-control noresize" name="observaciones" rows="3" placeholder="Ingresa Observaciones" title="Ingrese una observacion"></textarea>
			</div>
		</div>
		<div class="form-group text-center row">
			<div class="col-md-12">
				<button type="submit" class="btn btn-secondary">Enviar</button>
			</div>
		</div>
	</form>

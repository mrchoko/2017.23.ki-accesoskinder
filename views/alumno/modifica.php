	<form 
		id="form-alumno-modifica" name="form-alumno-modifica" 
		method="post" 
		action="./index.php?seccion=alumno&accion=modifica_bd&alumno_id=<?php echo $alumno_id; ?>" enctype="multipart/form-data">

		<div class="form-group row">
			<div class="col-md-6">
				<input 
					type="text" class="form-control" 
					name="nombre" placeholder="Ingresa Nombre" 
					required title="Ingrese un Nombre"
					value='<?php echo $alumno[0]['nombre']; ?>'>
			</div>
			<div class="col-md-6">
				<input 
					type="text" class="form-control" 
					name="apellido_paterno" placeholder="Ingresa Apellido Paterno" 
					required title="Ingrese Apellido Paterno"
					value='<?php echo $alumno[0]['apellido_paterno']; ?>'>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-6">
				<input 
					type="text" class="form-control" 
					name="apellido_materno" placeholder="Ingresa Apellido Materno" 
					required title="Ingrese Apellido Materno"
					value='<?php echo $alumno[0]['apellido_materno']; ?>'>
			</div>
			<div class="col-md-6">
                <div class='input-group date' id='divMiCalendario'>
                      <input type='text' id="fecha_nacimiento" name="fecha_nacimiento" class="form-control"  readonly value='<?php echo $alumno[0]['fecha_nacimiento']; ?>'/>                      
                      <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                      </span>
                </div>
	        </div>
	    </div>
		<div class="form-group row">
            <div class="col-md-6">
				<input class="form-control noresize" name="lugar_nacimiento" placeholder="Ingresa Lugar De Nacimiento" title="Ingrese un lugar de nacimiento" required value='<?php echo $alumno[0]['lugar_nacimiento']; ?>'/>
			</div>
			<div class="col-md-3 text-right">
				<input type="file" accept="image/png, image/jpeg" name="foto"/>
            </div>

            <div class="col-md-3 text-center">
            	<img class="foto" src="<?php echo './views/alumno/fotos/'.$alumno[0]['foto']; ?>">
            </div>
		</div>
		<div class="form-group row">
			<div class="col-md-6">
				<select name="genero" class="selectpicker" data-live-search="true" 
				title="Seleccione Genero" data-width="100%" 
				data-none-results-text="No se encontraron resultados" required>
			        	<option value="hombre" 
			        	<?php if($alumno[0]['genero'] == 'hombre') echo "selected";?>>Hombre</option>
			        	<option value="mujer" <?php if($alumno[0]['genero'] == 'mujer') echo "selected";?>>Mujer</option>
			    </select>
			</div>
			<div class="col-md-6">
				<select name="tipo_sangre" class="selectpicker" data-live-search="true" 
				title="Seleccione Tipo De Sangre" data-width="100%" 
				data-none-results-text="No se encontraron resultados" required>
			        	<option value="A Positiva" 
			        	<?php if($alumno[0]['tipo_sangre'] == 'A Positiva') echo "selected";?>
			        	>A Positiva</option>
			        	<option value="A Negativo"
			        	<?php if($alumno[0]['tipo_sangre'] == 'A Negativo') echo "selected";?>
			        	>A Negativo</option>
			        	<option value="B Positivo"
			        	<?php if($alumno[0]['tipo_sangre'] == 'B Positivo') echo "selected";?>
			        	>B Positivo</option>
			        	<option value="B Negativo"
			        	<?php if($alumno[0]['tipo_sangre'] == 'A Negativo') echo "selected";?>
			        	>B Negativo</option>
			        	<option value="O Positivo"
			        	<?php if($alumno[0]['tipo_sangre'] == 'O Positivo') echo "selected";?>
			        	>O Positivo</option>
			        	<option value="O Negativo"
			        	<?php if($alumno[0]['tipo_sangre'] == 'O Negativo') echo "selected";?>
			        	>O Negativo</option>
			        	<option value="AB Positivo"
			        	<?php if($alumno[0]['tipo_sangre'] == 'AB Positivo') echo "selected";?>
			        	>AB Positivo</option>
			        	<option value="AB Negativo"
			        	<?php if($alumno[0]['tipo_sangre'] == 'AB Negativo') echo "selected";?>
			        	>AB Negativo</option>
			    </select>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-6">
                <div class='input-group date' id='calendario_fecha_ingreso'>
                    <input type='text' id="fecha_ingreso" name="fecha_ingreso" class="form-control" placeholder="Ingresa Fecha De Ingreso" readonly value='<?php echo $alumno[0]['fecha_ingreso']; ?>'/>
                    <span class="input-group-addon">
                    	<span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
	        </div>
            <div class="col-md-6 text-center">
				<a href="index.php?seccion=alumno&accion=modifica&codigo=1&alumno_id=<?php echo $_GET['alumno_id']; ?>&nombre_codigo=<?php echo $alumno[0]['foto']; ?>">
  					<button type="button" class="btn btn-secondary">Cambiar Código de Barras</button>
				</a>
            </div>
        </div>
		<div class="form-group text-center row">
			<div class="col-md-12">
				<textarea class="form-control noresize" name="observaciones" rows="3" placeholder="Ingresa Observaciones" title="Ingrese una observacion" value='<?php echo $alumno[0]['observaciones']; ?>'></textarea>
			</div>
		</div>
		<div class="form-group text-center row">
			<div class="col-md-12">
				<button type="submit" class="btn btn-secondary">Enviar</button>
				<input type="hidden" name="foto_anterior" value='<?php echo $alumno[0]['foto']; ?>'>
				<input type='hidden' name='status' value='<?php echo $alumno[0]['status']; ?>'>
				<input type='hidden' name='codigo_barras' value='<?php echo $alumno[0]['codigo_barras']; ?>'>
			</div>
		</div>
	</form>
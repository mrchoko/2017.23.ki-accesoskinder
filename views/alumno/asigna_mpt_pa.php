	<form 
		id="form-asigna_mpt_pa-alta" name="form-asigna_mpt_pa-alta" 
		method="post" 
		action="">
		<div class="text-center">
            <h3>
            <?php echo $alumno[0]['nombre'].' '.$alumno[0]['apellido_paterno'].' '.$alumno[0]['apellido_materno']; ?>
            </h3>
        </div>
        

        <div class="form-group">
        	<div class="col-xs-6">

        		<div class="input-group">
					<span class="input-group-addon">Busqueda</span>
						<input id="busqueda-madre-padre-tutor" type="text" class="form-control" placeholder="Ingresa Busqueda">
				</div>

				<table class="table table-fixed">
					<thead>
						<th class="col-xs-12 text-center">Madre, Padre o Tutor</th>
					</thead>
					<tbody class="registros-madre-padre-tutor" id='chk-madre-padre-tutor'>
							
	                    	<?php
	                    	if(!empty($madre_padre_tutor)){
								foreach ($madre_padre_tutor as $key => $registros) {
									echo "<tr>";
					        		echo "<td class='col-xs-12'>";
					        		echo "<li class='list-group-item'>".$registros['nombre'].' '.$registros['apellido_paterno'].' '.$registros['apellido_materno'];
					        		echo "<div class='material-switch pull-right'>";
					        		echo "<input id='".$registros['nombre'].' '.$registros['apellido_paterno'].' '.$registros['apellido_materno']."' name='".$registros['id']."' type='checkbox' ";

					        		if(!empty($asigna_madre_padre_tutor)){
                                    $si_check = false;

                                    $nombre_alumno = $alumno[0]['nombre'].' '.$alumno[0]['apellido_paterno'].' '.$alumno[0]['apellido_materno'];
                                    $nombre_madre_padre_tutor = $registros['nombre'].' '.$registros['apellido_paterno'].' '.$registros['apellido_materno'];

                                    foreach ($asigna_madre_padre_tutor as $key => $valores) {

                                        if($nombre_alumno == $valores['nombre_alumno'] &&
                                            $nombre_madre_padre_tutor == $valores['nombre_madre_padre_tutor'] && $registros['id'] == $valores['madre_padre_tutor_id']){
                                            echo "value =".$valores['id']." name='".$registros['id']."' checked />";
                                            $si_check = true;
                                        }
                                    }
                                    if(!$si_check){echo "value='' />"; }
                                    }
                                    else{
                                        echo "value='' />";   
                                    }

					        		echo "<label for='".$registros['nombre'].' '.$registros['apellido_paterno'].' '.$registros['apellido_materno']."' class='label-primary'></label>";
					        		echo "</div>";
					        		echo "</li>";
					        		echo "</td>";
					        		echo "</tr>";
					        		
					        	}
					        }
					        ?>

                    </tbody>
                </table>

        	</div>

        	<div class="col-xs-6">

        		<div class="input-group">
					<span class="input-group-addon">Busqueda</span>
						<input id="busqueda-persona-autorizada" type="text" class="form-control" placeholder="Ingresa Busqueda">
				</div>

				<table class="table table-fixed">
					<thead>
						<th class="col-xs-12 text-center">Persona Autorizada</th>
					</thead>
					<tbody class="registros-persona-autorizada" id='chk-persona-autorizada'>
							
	                    	<?php
	                    	if(!empty($persona_autorizada)){
								foreach ($persona_autorizada as $key => $registros) {
									echo "<tr>";
					        		echo "<td class='col-xs-12'>";
					        		echo "<li class='list-group-item'>".$registros['nombre'].' '.$registros['apellido_paterno'].' '.$registros['apellido_materno'];
					        		echo "<div class='material-switch pull-right'>";
					        		echo "<input id='".$registros['nombre'].' '.$registros['apellido_paterno'].' '.$registros['apellido_materno']."' name='".$registros['id']."' type='checkbox' ";

					        		if(!empty($asigna_persona_autorizada)){
                                    $si_check = false;

                                    $nombre_alumno = $alumno[0]['nombre'].' '.$alumno[0]['apellido_paterno'].' '.$alumno[0]['apellido_materno'];
                                    $nombre_persona_autorizada = $registros['nombre'].' '.$registros['apellido_paterno'].' '.$registros['apellido_materno'];

                                    foreach ($asigna_persona_autorizada as $key => $valores) {

                                        if($nombre_alumno == $valores['nombre_alumno'] &&
                                            $nombre_persona_autorizada == $valores['nombre_persona_autorizada'] && $registros['id'] == $valores['persona_autorizada_id']){
                                            echo "value =".$valores['id']." name='".$registros['id']."' checked />";
                                            $si_check = true;
                                        }
                                    }
                                    if(!$si_check){echo "value='' />"; }
                                    }
                                    else{
                                        echo "value='' />";   
                                    }

					        		echo "<label for='".$registros['nombre'].' '.$registros['apellido_paterno'].' '.$registros['apellido_materno']."' class='label-primary'></label>";
					        		echo "</div>";
					        		echo "</li>";
					        		echo "</td>";
					        		echo "</tr>";
					        		
					        	}
					        }
					        ?>

                    </tbody>
                </table>

        	</div>



        </div>

		<div class="form-group text-center">
            <input type='hidden' id='alumno_id' value='<?php echo $alumno[0]['id']; ?>'>
        </div>

	</form>